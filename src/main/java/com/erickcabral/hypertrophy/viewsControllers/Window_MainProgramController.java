/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.viewsControllers;

import com.erickcabral.hypertrophy.application.Hypertrophy;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import com.erickcabral.hypertrophy.managers.WindowManager;

/**
 * FXML Controller class
 *
 * @author sexta
 */
public class Window_MainProgramController implements Initializable {

	@FXML
	AnchorPane mainAnchorPane;

	// -- File Menu -- //
	@FXML
	MenuItem menu_OpenAthlete;

	@FXML
	MenuItem menu_CloseProgram;

	// -- Athlete Menu -- //
	@FXML
	MenuItem menu_AddAthlete;

	// -- Training Menu -- //
	@FXML
	MenuItem menu_openDayBuilder;

	@FXML
	MenuItem menu_openWeekBuilder;

	// -- Exercise Menu -- //
	@FXML
	MenuItem menu_addExercise;

	//Initializes the controller class.
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		System.out.println("MAIN ANCHOR PANE: " + this.mainAnchorPane);
		Hypertrophy.getInstance().getPageManager().setMainAnchorPane(this.mainAnchorPane);

		this.menu_OpenAthlete.setOnAction((event) -> {
			Hypertrophy.getInstance().getWindowManager().openWindowShow(WindowManager.MAIN_OPEN, new Stage());
		});

		this.menu_AddAthlete.setOnAction((event) -> {
			Hypertrophy.getInstance().getWindowManager().addAthleteShow(WindowManager.MAIN_ADD, new Stage());
		});

		this.menu_addExercise.setOnAction((event) -> {
			Hypertrophy.getInstance().getWindowManager().trNewExerciseWindowShow(WindowManager.TRAINING_ADD, new Stage());
		});

		this.menu_openDayBuilder.setOnAction((event) -> {
			try {
				Hypertrophy.getInstance().getPageManager().loadDayTrainingBuilderPage();
			}
			catch (IOException ex) {
				Logger.getLogger(Window_MainProgramController.class.getName()).log(Level.SEVERE, null, ex);
			}
		});

		this.menu_openWeekBuilder.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					Hypertrophy.getInstance().getPageManager().loadWeekTrainingBuilderPage();
				}
				catch (IOException ex) {
					Logger.getLogger(Window_MainProgramController.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});

		this.menu_CloseProgram.setOnAction((event) -> {
			Hypertrophy.getInstance().getWindowManager().mainWindowClose();
		});
	}

	@FXML
	public void trainningCreator() {
		Hypertrophy.getInstance().getWindowManager().mainWindowHide();
		Hypertrophy.getInstance().getWindowManager().trainingMainWindowShow(WindowManager.TRAINING_BUILDER, null);
	}

	public void loadAthletePage() {
		try {
			Hypertrophy.getInstance().getPageManager().loadAthletePage();
		}
		catch (IOException ex) {
			Logger.getLogger(Window_MainProgramController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
