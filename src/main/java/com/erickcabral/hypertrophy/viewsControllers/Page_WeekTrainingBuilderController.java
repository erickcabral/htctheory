/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.viewsControllers;

import com.erickcabral.hypertrophy.application.Hypertrophy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import com.erickcabral.hypertrophy.model.DayTrainingSetBuilder;
import com.erickcabral.hypertrophy.model.WeekTrainingSetBuilder;
import com.erickcabral.hypertrophy.supportClasses.Plotters;
import com.erickcabral.hypertrophy.supportClasses.WeekDays;

/**
 * FXML Controller class
 *
 * @author Erick Cabral
 */
public class Page_WeekTrainingBuilderController implements Initializable {

	@FXML
	TextField tfWeekTrainingTag;
	@FXML
	ComboBox<WeekDays> cmbDays;
	@FXML
	ComboBox<String> cmbSetTag;

	@FXML
	Button btnAdd;
	@FXML
	Button btnRemove;
	@FXML
	Button btnTrSetSwitch;
	@FXML
	Button btnWeekSetSwitch;
	@FXML
	Button btnClear;
	@FXML
	Button btnClose;
	@FXML
	Button btnSave;

	@FXML
	TableView<Properties> tbvTrainingSets;
	@FXML
	TableColumn<Properties, String> columnDay;
	@FXML
	TableColumn<Properties, String> columnTrainingSetTag;

	@FXML
	PieChart pieChart;
	@FXML
	BarChart<String, Number> barChartMainMuscles;
	
	@FXML
	Label lbCaption;

	@FXML
	HBox hbMyLegend;

	private Plotters plotter = new Plotters();
	private Properties selectedTraining;
	int trainingSetIndex;
	private WeekTrainingSetBuilder weekSetBuilder = new WeekTrainingSetBuilder(Hypertrophy.getInstance().getWindowManager().getMainStage());

	WeekDays[] daysEnum = WeekDays.values();
	private ObservableList<WeekDays> daysList = FXCollections.observableArrayList(this.daysEnum);

	private ArrayList<DayTrainingSetBuilder> dayTrainingSetBuilderList = Hypertrophy.getInstance().getSerialization().getTrainingSetList();
	private ObservableList<String> trainingSetsTagList = FXCollections.observableArrayList();

	private boolean readyToSave = false;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO
		this.btnAdd.setDisable(true);
		this.btnClear.setDisable(true);
		this.cmbDays.setItems(this.daysList);
		this.cmbSetTag.setDisable(true);
		this.cmbSetTag.setItems(this.trainingSetsTagList);
		this.tbvTrainingSets.setDisable(true);

		for (DayTrainingSetBuilder getter : this.dayTrainingSetBuilderList) {
			this.trainingSetsTagList.add(getter.getTrainingTagName());
		}

		this.btnSave.setOnAction((event) -> {
			this.save();
		});

		this.btnClose.setOnAction((event) -> {
			Hypertrophy.getInstance().getPageManager().closePage();
		});

		this.tbvTrainingSets.getItems().addListener(new ListChangeListener<Properties>() {
			@Override
			public void onChanged(ListChangeListener.Change<? extends Properties> c) {
				if (c.getList().isEmpty()) {
					tbvTrainingSets.setDisable(true);
					columnDay.setCellValueFactory((value) -> new SimpleStringProperty(value.getValue().getProperty(DayTrainingSetBuilder.WEEK_DAY)));
					columnTrainingSetTag.setCellValueFactory((value) -> new SimpleStringProperty(value.getValue().getProperty(DayTrainingSetBuilder.TRAINING_TAG)));
					btnClear.setDisable(true);
				} else {
					tbvTrainingSets.setDisable(false);
					columnDay.setCellValueFactory((value) -> new SimpleStringProperty(value.getValue().getProperty(DayTrainingSetBuilder.WEEK_DAY)));
					columnTrainingSetTag.setCellValueFactory((value) -> new SimpleStringProperty(value.getValue().getProperty(DayTrainingSetBuilder.TRAINING_TAG)));
					btnClear.setDisable(false);
				}
			}
		});

		this.btnRemove.disableProperty().bind(this.tbvTrainingSets.getSelectionModel().selectedItemProperty().isNull());

		this.cmbDays.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<WeekDays>() {
			@Override
			public void changed(ObservableValue<? extends WeekDays> observable, WeekDays oldValue, WeekDays newValue) {
				if (newValue != null) {
					cmbSetTag.setDisable(false);
					weekSetBuilder.setWeekDay(newValue.toString());
				} else {
					cmbSetTag.setDisable(true);
				}
			}
		});

		this.cmbDays.getItems().addListener(new ListChangeListener<WeekDays>() {
			@Override
			public void onChanged(ListChangeListener.Change<? extends WeekDays> c) {
				cmbDays.setDisable(c.getList().isEmpty()); //Desabilita se Lista de Dias vazia
			}
		});

		this.cmbSetTag.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (newValue.intValue() >= 0) {
					btnAdd.setDisable(false);
					selectedTraining = dayTrainingSetBuilderList.get(newValue.intValue()).getDayTrainingSetProperties();
					loadPieChart(selectedTraining);
					loadBarChart(selectedTraining);
				} else {
					selectedTraining = null;
					btnAdd.setDisable(true);
				}
			}
		});

		this.btnAdd.setOnAction((event) -> {
			String selectedDay = this.cmbDays.getSelectionModel().getSelectedItem().toString();
			Properties training = (Properties) this.selectedTraining.clone();

			training.setProperty(DayTrainingSetBuilder.WEEK_DAY, selectedDay);
			this.tbvTrainingSets.getItems().add(training);
			Collections.sort(this.tbvTrainingSets.getItems(), WeekDays.dayComparatorByProperties);

			this.cmbDays.getSelectionModel().clearSelection();
			this.cmbSetTag.getSelectionModel().clearSelection();

			this.removeDayFromList(selectedDay);

		});

		this.btnRemove.setOnAction((event) -> {
			int selectedIndex = this.tbvTrainingSets.getSelectionModel().getSelectedIndex();
			String selectedDay = this.tbvTrainingSets.getItems().get(selectedIndex).getProperty(DayTrainingSetBuilder.WEEK_DAY);
			this.tbvTrainingSets.getItems().remove(selectedIndex);
			this.tbvTrainingSets.getSelectionModel().clearSelection();
			this.loadPieChart(this.selectedTraining);
			this.loadBarChart(this.selectedTraining);

			this.returnDayToList(selectedDay);
		});

		this.btnClear.setOnAction((event) -> {
			for (int i = 0; i < this.tbvTrainingSets.getItems().size(); i++) {
				this.returnDayToList(this.tbvTrainingSets.getItems().get(i).getProperty(DayTrainingSetBuilder.WEEK_DAY));
			}
			this.tbvTrainingSets.getItems().clear();
			this.pieChart.getData().clear();
			this.barChartMainMuscles.getData().clear();
		});

		this.pieChart.getData().addListener(new ListChangeListener<PieChart.Data>() {
			@Override
			public void onChanged(ListChangeListener.Change<? extends PieChart.Data> c) {
				for (PieChart.Data getter : c.getList()) {
					//System.out.println(String.format("Serie Antes: %s <NODE: %s>", getter.getName(), getter.getNode()));					
					getter.getNode().setId(plotter.getCSSGroupColors(getter.getName()));
					//	System.out.println(String.format("Serie Antes: %s <NODE: %s> : Style: %s", getter.getName(), getter.getNode(), getter.getNode().getStyle()));			 
				}
			}
		});
	}

	private void loadPieChart(Properties dayTrainingProperties) {
		ArrayList<Properties> selectedTrainingList = new ArrayList<>(this.tbvTrainingSets.getItems());
		this.pieChart.getData().clear();
		this.pieChart.getData().addAll(this.plotter.pieChartHotPlot(dayTrainingProperties, selectedTrainingList));
		this.plotter.showPieChartPercentages(this.pieChart);
	}

	private void loadBarChart(Properties dayTrainingProperties) {
		ArrayList<Properties> selectedTrainingList = new ArrayList<>(this.tbvTrainingSets.getItems());
		this.barChartMainMuscles.getData().clear();
		this.barChartMainMuscles.getData().addAll(this.plotter.barChartHotPlot(dayTrainingProperties, selectedTrainingList));
		this.plotter.addStyleId(this.barChartMainMuscles.getData());
	}

	private void removeDayFromList(String selectedDay) {
		for (WeekDays finder : this.daysList) {
			if (finder.toString().equals(selectedDay)) {
				this.daysList.remove(finder);
				return;
			}
		}
	}

	private void returnDayToList(String selectedDay) {
		for (WeekDays finder : WeekDays.values()) {
			if (finder.toString().equals(selectedDay)) {
				this.daysList.add(finder);
				Collections.sort(this.daysList, WeekDays.dayComparatorByWeekDayClass);
				return;
			}
		}
	}

	private void save() {
		ArrayList<Properties> weekTrainingList = new ArrayList<>(this.tbvTrainingSets.getItems());
		this.weekSetBuilder.setWeekTrainingList(weekTrainingList);
		int tagLength = this.tfWeekTrainingTag.getText().length();
		if (tagLength > 5 && !weekTrainingList.isEmpty()) {
			this.weekSetBuilder.setWeekTrainingTag(this.tfWeekTrainingTag.getText());
			this.weekSetBuilder.save(this.tfWeekTrainingTag.getText());
		} else {
			//TODO
		}
	}
}
