/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.viewsControllers;

import com.erickcabral.hypertrophy.application.Hypertrophy;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import com.erickcabral.hypertrophy.model.Calculations;
import com.erickcabral.hypertrophy.model.ExerciseParameters;
import com.erickcabral.hypertrophy.model.Muscles;
import com.erickcabral.hypertrophy.supportClasses.CheckBoxesHandler;
import com.erickcabral.hypertrophy.supportClasses.TitlePanesHandler;

/**
 *
 * @author sexta
 */
public class Window_TrainingAddController extends Muscles implements Initializable, Serializable {

	/////////////////// ComboBoxes //////////////
	@FXML
	ComboBox<String> cbBodyPart;
	@FXML
	ComboBox<String> cbMuscleGroup;
	@FXML
	ComboBox<String> cbMainMuscle;

	/////////////////// CHECKBOXES //////////////
	@FXML
	CheckBox chkShoulders;
	@FXML
	TextField tfShoulderGroup;

	@FXML
	CheckBox chkPectorals;
	@FXML
	TextField tfPectoralGroup;

	@FXML
	CheckBox chkBack;
	@FXML
	TextField tfBackGroup;

	@FXML
	CheckBox chkAbdominals;
	@FXML
	TextField tfAbdominalGroup;

	@FXML
	CheckBox chkBiceps;
	@FXML
	TextField tfBicepsGroup;

	@FXML
	CheckBox chkTriceps;
	@FXML
	TextField tfTricepsGroup;

	@FXML
	CheckBox chkGlutes;
	@FXML
	TextField tfGlutesGroup;

	@FXML
	CheckBox chkThighs;
	@FXML
	TextField tfThighsGroup;

	@FXML
	CheckBox chkCalves;
	@FXML
	TextField tfCalvesGroup;

	CheckBoxesHandler checkBoxesHandler = new CheckBoxesHandler();

/////////////////// Accordeaon and TitledPanes //////////////
	@FXML
	TitledPane tpShoulders;
	@FXML
	TitledPane tpPectorals;
	@FXML
	TitledPane tpBack;
	@FXML
	TitledPane tpAbdominals;
	@FXML
	TitledPane tpBiceps;
	@FXML
	TitledPane tpTriceps;
	@FXML
	TitledPane tpGlutes;
	@FXML
	TitledPane tpThighs;
	@FXML
	TitledPane tpCalves;

	TitlePanesHandler titlePanesHandler = new TitlePanesHandler();
/////////////////// BOTOES //////////////
	@FXML
	Button btnSave;
	@FXML
	Button btnClose;
	@FXML
	Button btnEdit;

	@FXML
	VBox vboxProps;
	@FXML
	VBox vboxCheckBoxes;
	@FXML
	VBox vboxTitlePanes;

	/*------------ TextFields -----------*/
	@FXML
	TextField tfExerciseName;
	/////////////////// TextFields SHOULDERS//////////////
	@FXML
	TextField tf_SH_Deltoid_Front;
	@FXML
	TextField tf_SH_Deltoid_Med;
	@FXML
	TextField tf_SH_Deltoid_Back;
	@FXML
	TextField tf_SH_Trapezius;
	@FXML
	TextField tf_SH_Infra;
	@FXML
	TextField tf_SH_Teres;

/////////////////// TextFields PECTORALS//////////////
	@FXML
	TextField tf_PEC_Upper;
	@FXML
	TextField tf_PEC_Major;
	@FXML
	TextField tf_PEC_Inner;
	@FXML
	TextField tf_PEC_Lower;

/////////////////// TextFields BACK//////////////
	@FXML
	TextField tf_BK_Dorsi;
	@FXML
	TextField tf_BK_Erector;

/////////////////// TextFields ABS//////////////
	@FXML
	TextField tf_ABS_Upper;
	@FXML
	TextField tf_ABS_Rectus;
	@FXML
	TextField tf_ABS_Lower;
	@FXML
	TextField tf_ABS_Oblique;

/////////////////// TextFields BICEPS//////////////
	@FXML
	TextField tf_BIC_LONG_HEAD;
	@FXML
	TextField tf_BIC_SHORT_HEAD;

/////////////////// TextFields TRICEPS//////////////
	@FXML
	TextField tf_TRI_LATERAL_HEAD;
	@FXML
	TextField tf_TRI_MEDIAL_HEAD;
	@FXML
	TextField tf_TRI_INNER_HEAD;

/////////////////// TextFields CALVES//////////////
	@FXML
	TextField tf_CAL_GAST_LAT_HEAD;
	@FXML
	TextField tf_CAL_GAST_INNER_HEAD;

	/////////////////// TextFields THIGHS//////////////
	@FXML
	TextField tf_THG_RETUS_FEMUROUS;
	@FXML
	TextField tf_THG_VASTUS_LATERALIS;
	@FXML
	TextField tf_THG_VASTUS_MEDIALIS;
	@FXML
	TextField tf_THG_ADDUCTOR;
	@FXML
	TextField tf_THG_ABDUCTOR;
	@FXML
	TextField tf_THG_BICEPS_FEMORIS;

	/////////////////// TextFields GLUTES //////////////
	@FXML
	TextField tf_GLUTEUS_MAXIMUS;
	@FXML
	TextField tf_GLUTEUS_MEDIUS;

/////////////////// CLASSES INSTANCIATED//////////////
	private final String EXERCISE_WARNING = "Write an Exercise Name to proceed";

	// ExerciseParameters exerciseParameters = new ExerciseParameters();
	ExerciseParameters exerciseParameters = new ExerciseParameters();
	private Properties exerciseProperties = new Properties();
	Muscles muscles = new Muscles();
	private Calculations calculations = new Calculations();

	/////////////////// GLOBAL VARIABLES////////////// 
	private String SELECTED_BODYPART;
	private String SELECTED_MUSCLE_GROUP;
	private String SELECTED_MAIN_MUSCLE;

	EventHandler<ActionEvent> selectBoxEvent = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			CheckBox chkTriggedBox = (CheckBox) event.getSource();
			boolean isSelected = chkTriggedBox.isSelected();
			String group = chkTriggedBox.getId();
			System.out.println("ID>> " + chkTriggedBox.getId() + " Selected >> " + isSelected);
			checkBoxesHandler.setSecondaryGroupEnabled(group, isSelected);
		}
	};

	EventHandler<KeyEvent> nameKeyEvent = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {
			TextField tf = (TextField) event.getSource();
			if (!tf.getText().equals(EXERCISE_WARNING) && tf.getText().length() > 4) {
				cbBodyPart.setDisable(false);
			} else {
				cbBodyPart.setDisable(true);
			}
		}
	};

	private TextField mainGroupTextField;
	private TextField activeMuscleTextField;

	EventHandler<KeyEvent> valueFilterEvent = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {
			TextField secondaryTextField = (TextField) event.getSource();
			String secondaryTF_ID = secondaryTextField.getId();
			String group;

			boolean isGroup = false;
			for (String finder : Muscles.STRING_LIST_GROUPS) {
				if (finder.equals(secondaryTF_ID)) {
					isGroup = true;
				}
			}

			if (calculations.isIntFilterValid(isGroup, event)) {
				int groupValue;
				int tfValueInt;
				if (secondaryTextField.getText().equals("")) {
					tfValueInt = 0;
				} else {
					tfValueInt = Integer.valueOf(secondaryTextField.getText());
				}
				TextField fixedTextField;
				String secondaryGroup;
				boolean isSelectedGroup = false;
				ArrayList<TextField> tfList;

				if (isGroup) {
					fixedTextField = mainGroupTextField;
					secondaryGroup = secondaryTF_ID;
					groupValue = Integer.valueOf(fixedTextField.getText());
					tfList = checkBoxesHandler.getSelecTextFields();
					checkBoxesHandler.updateBoxSetConfig(secondaryGroup);
					if (secondaryTextField.getText().isEmpty()) {
						titlePanesHandler.setSecondaryTitlePaneEnabled(secondaryGroup, false);
					} else {
						titlePanesHandler.setSecondaryTitlePaneEnabled(secondaryGroup, true);
					}
				} else {
					fixedTextField = activeMuscleTextField;
					secondaryGroup = titlePanesHandler.getTitlePaneGroupByTextField(secondaryTF_ID);
					groupValue = checkBoxesHandler.getGroupValue(secondaryGroup);
					if (titlePanesHandler.getTitlePaneGroupByTextField(secondaryTF_ID).equals(SELECTED_MUSCLE_GROUP)) {
						isSelectedGroup = true;
					}
					tfList = titlePanesHandler.getTextFieldListByMuscle(secondaryTF_ID);
				}

				int validInput = calculations.inputValueUpdate(isGroup, isSelectedGroup, fixedTextField, secondaryTextField, tfList).getValidInput();
				int primaryUpdate = calculations.inputValueUpdate(isGroup, isSelectedGroup, fixedTextField, secondaryTextField, tfList).getPrimaryUpdate();

				if (validInput != tfValueInt) {
					secondaryTextField.clear();
					String validInpuToString = String.valueOf(validInput);
					secondaryTextField.setText(validInpuToString);
				}
				//Atualiza Campo Principal.
				String primaryUpdateToString = String.valueOf(primaryUpdate);
				if (isGroup) {
					fixedTextField.clear();
					fixedTextField.setText(primaryUpdateToString);
				} else {
					if (titlePanesHandler.getTitlePaneGroupByTextField(secondaryTF_ID).equals(SELECTED_MUSCLE_GROUP)) {
						System.out.println("IS THE SELECTED PANEL");
						fixedTextField.clear();
						fixedTextField.setText(primaryUpdateToString);
					}
					titlePanesHandler.updateTitlePaneBySum(secondaryTF_ID, SELECTED_MUSCLE_GROUP);
				}
			}
		}
	};

	private final String BODY_PART_PROMPT = "Select Body Part";
	ObservableList<String> BODY_PART_LIST = FXCollections.observableArrayList();
	private final String MUSCLE_GROUP_PROMPT = "Select Muscle Group";
	ObservableList<String> MUSCLE_GROUP_LIST = FXCollections.observableArrayList();
	private final String MAIN_MUSCLE_PROMPT = "Select Main Muscle";
	ObservableList<String> MAIN_MUSCLE_LIST = FXCollections.observableArrayList();

	//// INITIALIZATION /////
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		this.BODY_PART_LIST.addAll(this.BODY_PART_STRING_LIST);

		this.tfExerciseName.setPromptText(EXERCISE_WARNING);
		this.tfExerciseName.setOnKeyTyped(nameKeyEvent);

		setCheckBoxesHandler();
		setTitledPaneHandler();
		titlePanesHandler.disableTitlePanes();

		btnSave.setDisable(true);
		btnClose.setDisable(false);
		btnEdit.setDisable(true);

		cbBodyPart.setDisable(true);
		cbMuscleGroup.setDisable(true);
		cbMainMuscle.setDisable(true);

		if (tfExerciseName.getPromptText().equals(this.EXERCISE_WARNING)) {
			cbBodyPart.setItems(this.BODY_PART_LIST);
		} else {
			//TODO:
		}

		cbBodyPart.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				checkBoxesHandler.resetBoxConfig();
				MUSCLE_GROUP_LIST.clear();
				MUSCLE_GROUP_LIST.add(MUSCLE_GROUP_PROMPT);
				if (newValue == null || newValue.equals(BODY_PART_PROMPT)) {
					cbMuscleGroup.setValue(MUSCLE_GROUP_PROMPT);
					cbMuscleGroup.setDisable(true);
					cbMainMuscle.setValue(MAIN_MUSCLE_PROMPT);
					cbMainMuscle.setDisable(true);
				} else {
					SELECTED_BODYPART = newValue;
					MUSCLE_GROUP_LIST.addAll(getMuscleGroupStringList(SELECTED_BODYPART));
					cbMuscleGroup.setDisable(false);
					cbMuscleGroup.setItems(MUSCLE_GROUP_LIST);
					cbMuscleGroup.setValue(MUSCLE_GROUP_PROMPT);
				}
			}
		});
		cbMuscleGroup.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				checkBoxesHandler.resetBoxConfig();
				MAIN_MUSCLE_LIST.clear();
				MAIN_MUSCLE_LIST.add(MAIN_MUSCLE_PROMPT);
				SELECTED_MUSCLE_GROUP = newValue;
				if (SELECTED_MUSCLE_GROUP == null) {
					SELECTED_MUSCLE_GROUP = MUSCLE_GROUP_PROMPT;
				}
				if (SELECTED_MUSCLE_GROUP.equals(MUSCLE_GROUP_PROMPT)) {
					cbMainMuscle.setValue(MAIN_MUSCLE_PROMPT);
					cbMainMuscle.setDisable(true);
				} else {
					cbMainMuscle.setDisable(false);
					MAIN_MUSCLE_LIST.addAll(getMainMusclesStringList(SELECTED_MUSCLE_GROUP));
					cbMainMuscle.setItems(MAIN_MUSCLE_LIST);
					cbMainMuscle.setValue(MAIN_MUSCLE_PROMPT);
				}
			}
		});
		cbMainMuscle.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				SELECTED_MAIN_MUSCLE = newValue;
				if (SELECTED_MAIN_MUSCLE == null) {
					SELECTED_MAIN_MUSCLE = MAIN_MUSCLE_PROMPT;
				}
				//TODO: REVISAR
				if (!SELECTED_MAIN_MUSCLE.equals(MAIN_MUSCLE_PROMPT)) {
					checkBoxesHandler.setMainGroup(SELECTED_MUSCLE_GROUP);
					mainGroupTextField = checkBoxesHandler.getMainGroupBoxSet().getTextField();
					activeMuscleTextField = titlePanesHandler.getTitlePaneTextField(SELECTED_MUSCLE_GROUP, SELECTED_MAIN_MUSCLE);

					titlePanesHandler.setMainTitlePaneEnabled(SELECTED_MUSCLE_GROUP, SELECTED_MAIN_MUSCLE);

					btnSave.setDisable(false);
				}
			}
		});
	}

///////////////////////////////////////// BUTTONS ////////////////////////////////////////////////
	public void close() {
		Hypertrophy.getInstance().getWindowManager().trNewExerciseWindowClose();
	}

	public void edit() {
		//TODO: sobrescrever o arquivo salvo quando Salvar

		btnSave.setDisable(false);
		btnEdit.setDisable(true);
		btnClose.setText("Cancel");

		vboxCheckBoxes.setDisable(false);
		vboxProps.setDisable(false);
		vboxTitlePanes.setDisable(false);
	}
	public void save() {
		
		//TODO - HABILITAR BOTAO SAVE SOMENTE QDO TODOS PARAMETROS ESTIVEREM OK.
		
		this.exerciseParameters.setExerciseName(this.tfExerciseName.getText());
		this.exerciseParameters.setExerciseBodyPart(this.SELECTED_BODYPART);
		this.exerciseParameters.setExerciseMuscleGroup(this.SELECTED_MUSCLE_GROUP);
		this.exerciseParameters.setExerciseMainMuscle(this.SELECTED_MAIN_MUSCLE);
		
		this.exerciseParameters.setGroupPercentages(this.checkBoxesHandler.getGroupProperties());
		this.exerciseParameters.setMainMusclesPercentages(this.titlePanesHandler.getMuscleProperties());
		
		
		/* somente teste - apagar isso -> */ this.exerciseParameters.getRealMainMusclePercentages();
		
		boolean isSaved = exerciseParameters.saveExerciseParameters();	

		if (isSaved) {
			btnSave.setDisable(true);
			btnEdit.setDisable(false);
			btnClose.setText("Close");

			vboxCheckBoxes.setDisable(true);
			vboxProps.setDisable(true);
			vboxTitlePanes.setDisable(true);
		} else {
			// TODO: MENSAGEM DE ERRO....
			System.out.println("ERRO AO SALVAR ARQUIVO");
		}
	}

	///////////////////////////////////// HANDLERS ///////////////////////////////////
	public void setCheckBoxesHandler() {
		checkBoxesHandler.addCheckBoxSet(chkShoulders, Muscles.SHOULDERS, tfShoulderGroup);
		checkBoxesHandler.addCheckBoxSet(chkPectorals, Muscles.PECTORALS, tfPectoralGroup);
		checkBoxesHandler.addCheckBoxSet(chkBack, Muscles.BACK, tfBackGroup);
		checkBoxesHandler.addCheckBoxSet(chkAbdominals, Muscles.ABS, tfAbdominalGroup);
		checkBoxesHandler.addCheckBoxSet(chkBiceps, Muscles.BICEPS, tfBicepsGroup);
		checkBoxesHandler.addCheckBoxSet(chkTriceps, Muscles.TRICEPS, tfTricepsGroup);
		checkBoxesHandler.addCheckBoxSet(chkGlutes, Muscles.GLUTES, tfGlutesGroup);
		checkBoxesHandler.addCheckBoxSet(chkThighs, Muscles.THIGHS, tfThighsGroup);
		checkBoxesHandler.addCheckBoxSet(chkCalves, Muscles.CALVES, tfCalvesGroup);

		this.checkBoxesHandler.setBoxSetsEventHandler(selectBoxEvent, valueFilterEvent);
		this.checkBoxesHandler.resetBoxConfig();
	}

	public void setTitledPaneHandler() {
		titlePanesHandler.addTitledPanes(tpShoulders, Muscles.SHOULDERS);
		titlePanesHandler.addTpTextField(tpShoulders, tf_SH_Deltoid_Front, Muscles.SH_DELTOID_FRONT);
		titlePanesHandler.addTpTextField(tpShoulders, tf_SH_Deltoid_Med, Muscles.SH_DELTOID_MED);
		titlePanesHandler.addTpTextField(tpShoulders, tf_SH_Deltoid_Back, Muscles.SH_DELTOID_BACK);
		titlePanesHandler.addTpTextField(tpShoulders, tf_SH_Trapezius, Muscles.SH_TRAPEZIUS);
		titlePanesHandler.addTpTextField(tpShoulders, tf_SH_Infra, Muscles.SH_INFRASPINATUS);
		titlePanesHandler.addTpTextField(tpShoulders, tf_SH_Teres, Muscles.SH_TERES);

		titlePanesHandler.addTitledPanes(tpPectorals, Muscles.PECTORALS);
		titlePanesHandler.addTpTextField(tpPectorals, tf_PEC_Inner, Muscles.PEC_INNER);
		titlePanesHandler.addTpTextField(tpPectorals, tf_PEC_Lower, Muscles.PEC_LOWER);
		titlePanesHandler.addTpTextField(tpPectorals, tf_PEC_Major, Muscles.PEC_MAJOR);
		titlePanesHandler.addTpTextField(tpPectorals, tf_PEC_Upper, Muscles.PEC_UPPER);

		titlePanesHandler.addTitledPanes(tpBack, Muscles.BACK);
		titlePanesHandler.addTpTextField(tpBack, tf_BK_Dorsi, Muscles.BAK_DORSI);
		titlePanesHandler.addTpTextField(tpBack, tf_BK_Erector, Muscles.BAK_ERECTOR);

		titlePanesHandler.addTitledPanes(tpAbdominals, Muscles.ABS);
		titlePanesHandler.addTpTextField(tpAbdominals, tf_ABS_Lower, Muscles.ABS_LOWER);
		titlePanesHandler.addTpTextField(tpAbdominals, tf_ABS_Oblique, Muscles.ABS_OBLIQUE);
		titlePanesHandler.addTpTextField(tpAbdominals, tf_ABS_Rectus, Muscles.ABS_RECTUS);
		titlePanesHandler.addTpTextField(tpAbdominals, tf_ABS_Upper, Muscles.ABS_UPPER);

		titlePanesHandler.addTitledPanes(tpBiceps, Muscles.BICEPS);
		titlePanesHandler.addTpTextField(tpBiceps, tf_BIC_LONG_HEAD, Muscles.BIC_LONG_HEAD);
		titlePanesHandler.addTpTextField(tpBiceps, tf_BIC_SHORT_HEAD, Muscles.BIC_SHORT_HEAD);

		titlePanesHandler.addTitledPanes(tpTriceps, Muscles.TRICEPS);
		titlePanesHandler.addTpTextField(tpTriceps, tf_TRI_INNER_HEAD, Muscles.TRI_INNER_HEAD);
		titlePanesHandler.addTpTextField(tpTriceps, tf_TRI_LATERAL_HEAD, Muscles.TRI_LATERAL_HEAD);
		titlePanesHandler.addTpTextField(tpTriceps, tf_TRI_MEDIAL_HEAD, Muscles.TRI_MEDIAL_HEAD);

		titlePanesHandler.addTitledPanes(tpGlutes, Muscles.GLUTES);
		titlePanesHandler.addTpTextField(tpGlutes, tf_GLUTEUS_MAXIMUS, Muscles.GL_MAXIMUS);
		titlePanesHandler.addTpTextField(tpGlutes, tf_GLUTEUS_MEDIUS, Muscles.GL_MEDIUS);

		titlePanesHandler.addTitledPanes(tpThighs, Muscles.THIGHS);
		titlePanesHandler.addTpTextField(tpThighs, tf_THG_ABDUCTOR, Muscles.THG_ABDUCTOR);
		titlePanesHandler.addTpTextField(tpThighs, tf_THG_ADDUCTOR, Muscles.THG_ADDUCTOR);
		titlePanesHandler.addTpTextField(tpThighs, tf_THG_BICEPS_FEMORIS, Muscles.THG_BICEPS_FEMORIS);
		titlePanesHandler.addTpTextField(tpThighs, tf_THG_RETUS_FEMUROUS, Muscles.THG_RETUS_FEMUROUS);
		titlePanesHandler.addTpTextField(tpThighs, tf_THG_VASTUS_LATERALIS, Muscles.THG_VASTUS_LATERALIS);
		titlePanesHandler.addTpTextField(tpThighs, tf_THG_VASTUS_MEDIALIS, Muscles.THG_VASTUS_MEDIALIS);

		titlePanesHandler.addTitledPanes(tpCalves, Muscles.CALVES);
		titlePanesHandler.addTpTextField(tpCalves, tf_CAL_GAST_INNER_HEAD, Muscles.CAL_GAST_INNER_HEAD);
		titlePanesHandler.addTpTextField(tpCalves, tf_CAL_GAST_LAT_HEAD, Muscles.CAL_GAST_LAT_HEAD);

		this.titlePanesHandler.setTitlePaneEventHandlers(valueFilterEvent);
	}
}
