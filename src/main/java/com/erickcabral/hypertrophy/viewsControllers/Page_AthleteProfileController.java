/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.viewsControllers;

import com.erickcabral.hypertrophy.application.Hypertrophy;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import com.erickcabral.hypertrophy.model.AthleteProfile;
import com.erickcabral.hypertrophy.model.Muscles;
import com.erickcabral.hypertrophy.supportClasses.InputEvents;
import com.erickcabral.hypertrophy.supportClasses.LineChartPlotters;
import com.erickcabral.hypertrophy.supportClasses.TextFieldHandler;

/**
 * FXML Controller class
 *
 * @author Erick Cabral
 */
public class Page_AthleteProfileController implements Initializable {

	/////////////////// TextFields //////////////
	@FXML
	Label lbID;
	@FXML
	DatePicker dpInicial;
	@FXML
	TextField tfName;
	@FXML
	TextField tfSurname;
	@FXML
	TextField tfWeight;
	@FXML
	TextField tfHeight;
	@FXML
	TextField tfFat;
	@FXML
	TextField tfShouders;
	@FXML
	TextField tfChest;
	@FXML
	TextField tfWaist;
	@FXML
	TextField tfGlutes;
	@FXML
	TextField tfLeftArm;
	@FXML
	TextField tfRightArm;
	@FXML
	TextField tfLefThigh;
	@FXML
	TextField tfRightThigh;
	@FXML
	TextField tfLeftCalf;
	@FXML
	TextField tfRightCalf;
	/////////////////// TextFields UPDATES //////////////

	@FXML
	DatePicker dpUpdate;
	@FXML
	TextField tfWeightUP;
	@FXML
	TextField tfHeightUP;
	@FXML
	TextField tfFatUP;
	@FXML
	TextField tfShoudersUP;
	@FXML
	TextField tfChestUP;
	@FXML
	TextField tfWaistUP;
	@FXML
	TextField tfGlutesUP;
	@FXML
	TextField tfLeftArmUP;
	@FXML
	TextField tfRightArmUP;
	@FXML
	TextField tfLefThighUP;
	@FXML
	TextField tfRightThighUP;
	@FXML
	TextField tfLeftCalfUP;
	@FXML
	TextField tfRightCalfUP;

	//////////////////////// LineCherts ///////////////////
	@FXML
	LineChart<String, Float> lcWeight;
	@FXML
	LineChart<String, Float> lcFat;
	@FXML
	LineChart<String, Float> lcShoulder;
	@FXML
	LineChart<String, Float> lcChest;
	@FXML
	LineChart<String, Float> lcWaist;
	@FXML
	LineChart<String, Float> lcGlutes;

	@FXML
	LineChart<String, Float> lcLeftArm;
	@FXML
	LineChart<String, Float> lcRightArm;
	@FXML
	LineChart<String, Float> lcLeftThigh;
	@FXML
	LineChart<String, Float> lcRightThigh;
	@FXML
	LineChart<String, Float> lcLeftCalf;
	@FXML
	LineChart<String, Float> lcRightCalf;

	@FXML
	Button btnNewUpdate;
	@FXML
	Button btnEditUpdate;
	@FXML
	Button btnLoadUpdate;

	@FXML
	Button btnEditInicial;
	@FXML
	Button btnCancelInitial;

	private final String BTN_CANCEL = "Cancel";
	private final String BTN_EDIT = "Edit";

	private final String BTN_SAVE = "Save";
	private final String BTN_ADD = "Add";
	private final String BTN_NEW = "New";

	AthleteProfile athleteProfile;

	TextFieldHandler inicialTextFieldsHandler = new TextFieldHandler();
	TextFieldHandler updateTextFieldsHandler = new TextFieldHandler();

	int displayedUpdatePosition;

	EventHandler<KeyEvent> floatFilter = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {
			TextField source = (TextField) event.getSource();
			String inputValue = source.getText();
			if (inputValue.length() > 2 && inputValue.length() < 6) {
				if (Float.valueOf(inputValue) == 0f) {
					source.selectAll();
				}
			} else {
				if (inputValue.length() < 6 && !inputValue.isEmpty()) {
					if (!event.getCharacter().equals("\b")) {
						String valid = InputEvents.floatMask(inputValue, 3);
						source.setText(valid);
						source.end();
					}
				} else {
					source.deletePreviousChar();
					source.setText(source.getText());
					source.end();
				}
			}
		}
	};
	EventHandler<KeyEvent> nameEvent = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {
			TextField source = (TextField) event.getSource();
			String inputValue = source.getText();
			if (inputValue.length() > 3) {
				System.out.println("Name Ok - PRONTO PRA SALVAR");
			}
		}
	};

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {

		System.out.println("ATHLETE PAGE LOADING");
		this.athleteProfile = Hypertrophy.getInstance().getSelectedProfile();
		System.out.println("SELECTED PROFILE -> " + this.athleteProfile.getAthleteName());
		this.loadProfile(athleteProfile);
		
		this.tfName.setDisable(true);
		this.tfSurname.setDisable(true);
		this.tfName.setOnKeyTyped(nameEvent);
		this.tfSurname.setOnKeyTyped(nameEvent);

		setInicialTfList();
		setUpdateslTfList();
		this.inicialTextFieldsHandler.setAllTextFieldsDisabled(true);
		this.updateTextFieldsHandler.setAllTextFieldsDisabled(true);

		dpInicial.setDisable(true);
		dpUpdate.setDisable(true);

		btnNewUpdate.setDisable(true);
		btnEditUpdate.setDisable(true);
		btnEditInicial.setDisable(true);

		btnEditInicial.setOnAction((event) -> {
			boolean isEditable = this.btnEditInicial.getText().equals(BTN_EDIT);
			this.editInicial(isEditable);
		});

		this.btnCancelInitial.setOnAction((event) -> {
			this.cancelAction(false);
		});

		this.btnEditUpdate.setOnAction((event) -> {
			boolean isEditable = this.btnEditUpdate.getText().equals(BTN_EDIT);
			System.out.println("GET TEXT " + this.btnEditUpdate.getText() + "bool " + isEditable);

			this.editUpdate(this.btnEditUpdate.getText().equals(BTN_EDIT));
		});

		this.btnLoadUpdate.setOnAction((event) -> {
			this.loadUpdate();
		});

		this.btnNewUpdate.setOnAction((event) -> {
			this.addUpdate();
		});
		
		
	}

	private void unlockFields(boolean isUpdate, boolean isEditable) {
		if (isUpdate) {
			System.out.println("IsEditable > " + isEditable);
			this.updateTextFieldsHandler.setAllTextFieldsDisabled(!isEditable);
			this.dpUpdate.setDisable(!isEditable);
		} else {
			this.inicialTextFieldsHandler.setAllTextFieldsDisabled(!isEditable);
			this.tfName.setDisable(!isEditable);
			this.tfSurname.setDisable(!isEditable);
			this.dpInicial.setDisable(!isEditable);
		}

	}

	public void editInicial(boolean isEditable) {
		this.unlockFields(false, isEditable);  //TRUE to Unlock TextFields and DatePicker
		btnEditInicial.setText(isEditable ? BTN_SAVE : BTN_EDIT); //editMode On: BtnAdd->BtnSave Off:BtnAdd->BtnNew
		btnCancelInitial.setDisable(!isEditable); //editMode On: BtnCancel ->on Off:BtnCancel ->off	
		if (!isEditable) {
			this.athleteProfile.saveMeasureEdition(this.getInicialMeasures(), 0);
			this.loadProfile(athleteProfile);
		}
	}

	public void cancelAction(boolean isUpdate) {
		this.unlockFields(isUpdate, false); //TRUE to Unlock TextFields and DatePicker
		if (isUpdate) {
			this.btnNewUpdate.setText(BTN_NEW);
			this.btnEditUpdate.setText(BTN_EDIT);
			System.out.println("CANCELAMENTO DE EDIÇÃO UPDATE");
		} else {
			this.btnCancelInitial.setDisable(true);
			this.btnEditInicial.setText(BTN_EDIT);
			System.out.println("CANCELAMENTO DE EDIÇÃO INICIAL");
		}
		this.loadProfile(Hypertrophy.getInstance().getSelectedProfile());
	}

	public void addUpdate() {
		switch (this.btnNewUpdate.getText()) {
		case BTN_NEW: //New Update
			dpUpdate.setValue(LocalDate.now());

			this.unlockFields(true, true);  //TRUE to Unlock TextFields and DatePicker

			updateTextFieldsHandler.clearTextFields();
			btnNewUpdate.setText(BTN_ADD);
			btnEditUpdate.setText(BTN_CANCEL);
			break;
		case BTN_ADD: //Confirm New Update
			btnNewUpdate.setText(BTN_NEW);
			btnEditUpdate.setText(BTN_EDIT);

			this.unlockFields(true, false);

			this.athleteProfile.getInicialMeasures(this.getUpdateMeasures());
			this.loadProfile(athleteProfile);
			break;

		case BTN_SAVE: //Save Edition
			this.unlockFields(true, false);
			btnEditUpdate.setText(BTN_EDIT);
			btnNewUpdate.setText(BTN_NEW);
			this.athleteProfile.saveMeasureEdition(this.getUpdateMeasures(), this.displayedUpdatePosition);
			this.loadProfile(athleteProfile);
			break;
		}
	}

	public void editUpdate(boolean isEditable) { //EDITAR UPDATES	
		if (isEditable) {
			System.out.println(" EDITION POSITION > " + this.displayedUpdatePosition);
			this.unlockFields(true, isEditable); //Unlock TextFields and DatePicker
			this.btnNewUpdate.setText(BTN_SAVE);
			this.btnEditUpdate.setText(BTN_CANCEL);
		} else {
			this.cancelAction(true);
		}
		/*
		switch (btnEditUpdate.getText()) {
		case BTN_EDIT:
			System.out.println(" EDITION POSITION > " + this.displayedUpdatePosition);
			//this.updateTextFieldsHandler.setAllTextFieldsDisabled(false);
			//this.dpUpdate.setDisable(false); //DatePick On for Editable Off for NonEditable

			this.unlockFields(true, false); //Unlock TextFields and DatePicker

			this.btnNewUpdate.setText(BTN_SAVE);
			this.btnEditUpdate.setText(BTN_CANCEL);
			break;
		case BTN_CANCEL:
			//this.updateTextFieldsHandler.setAllTextFieldsDisabled(true);
			//this.dpUpdate.setDisable(true); //DatePick On for Editable Off for NonEditable

			this.unlockFields(true, true); //Unlock TextFields and DatePicker

			this.btnNewUpdate.setText(BTN_NEW);
			this.btnEditUpdate.setText(BTN_EDIT);
			this.cancelAction();
			break;
		}
		 */
	}

	public void loadUpdate() {
		//TODO: Open a table of saved Updates to Load it.
		System.out.println("TODO: LOAD UPDATES");
	}

	private Properties getUpdateMeasures() {
		Properties updateProps = new Properties();
		updateProps.setProperty(AthleteProfile.DATE_INPUT, this.dpUpdate.getValue().format(DateTimeFormatter.ISO_DATE));
		for (TextField getter : this.updateTextFieldsHandler.getTextFieldList()) {
			updateProps.setProperty(getter.getId(), getter.getText());
		}
		System.out.println("NEW UPDATE -> " + updateProps);
		return updateProps;
	}

	private Properties getInicialMeasures() {
		Properties inicialProps = new Properties();
		inicialProps.setProperty(AthleteProfile.ATHLETE_NAME, this.tfName.getText());
		inicialProps.setProperty(AthleteProfile.ATHLETE_SURNAME, this.tfSurname.getText());
		inicialProps.setProperty(AthleteProfile.DATE_INPUT, this.dpInicial.getValue().format(DateTimeFormatter.ISO_DATE));
		for (TextField getter : this.inicialTextFieldsHandler.getTextFieldList()) {
			inicialProps.setProperty(getter.getId(), getter.getText());
		}
		System.out.println("NEW UPDATE -> " + inicialProps);
		return inicialProps;
	}



	public void loadProfile(AthleteProfile profile) {
		this.athleteProfile = profile;
		lbID.setText(String.valueOf(profile.getAthleteID()));
		tfName.setText(profile.getAthleteName());
		tfSurname.setText(profile.getAthleteSurname());

		//Carregamento dos Dados Iniciais	
		String inicialDate = profile.getMeasuresList().get(0).getProperty(athleteProfile.DATE_INPUT);
		LocalDate inicial = LocalDate.parse(inicialDate, DateTimeFormatter.ISO_DATE);
		dpInicial.setValue(inicial);
		this.inicialTextFieldsHandler.writeDataOnTextFields(profile.getMeasuresList().get(0)); //Escreve dados nos Campos

		//Carregamento do Ultimo Update
		this.displayedUpdatePosition = profile.getMeasuresList().size() - 1;
		int lastUpdate = displayedUpdatePosition;
		String updateDate = profile.getMeasuresList().get(lastUpdate).getProperty(athleteProfile.DATE_INPUT);
		LocalDate updated = LocalDate.parse(updateDate, DateTimeFormatter.ISO_DATE);
		dpUpdate.setValue(updated);
		this.updateTextFieldsHandler.writeDataOnTextFields(profile.getMeasuresList().get(lastUpdate)); //Escreve dados nos Campos

		btnNewUpdate.setDisable(false);
		btnEditUpdate.setDisable(false);
		btnEditInicial.setDisable(false);

		//---LOG DATAS---//
		for (Properties x : profile.getMeasuresList()) {
			System.out.println(String.format("MEASURES -> %s", x.getProperty(AthleteProfile.DATE_INPUT)));
		}
		showGeneralGraphs();
	}

	private void showGeneralGraphs() {
		AthleteProfile profile = Hypertrophy.getInstance().getSelectedProfile();

		LineChartPlotters lineChartPlotters = new LineChartPlotters(profile);
		this.lineChartClear();

		//Body Measures//
		this.lcWeight.getData().addAll(lineChartPlotters.getInicialSerie(AthleteProfile.ATHLETE_WEIGHT), lineChartPlotters.getUpdatedSerie(AthleteProfile.ATHLETE_WEIGHT));
		this.lcFat.getData().addAll(lineChartPlotters.getInicialSerie(AthleteProfile.ATHLETE_FAT), lineChartPlotters.getUpdatedSerie(AthleteProfile.ATHLETE_FAT));

		//Torax Measures//		
		this.lcShoulder.getData().addAll(lineChartPlotters.getInicialSerie(Muscles.SHOULDERS), lineChartPlotters.getUpdatedSerie(Muscles.SHOULDERS));
		this.lcChest.getData().addAll(lineChartPlotters.getInicialSerie(Muscles.TORAX), lineChartPlotters.getUpdatedSerie(Muscles.TORAX));
		this.lcWaist.getData().addAll(lineChartPlotters.getInicialSerie(Muscles.WAIST), lineChartPlotters.getUpdatedSerie(Muscles.WAIST));
		this.lcGlutes.getData().addAll(lineChartPlotters.getInicialSerie(Muscles.GLUTES), lineChartPlotters.getUpdatedSerie(Muscles.GLUTES));

		//Arms Measures//	
		this.lcLeftArm.getData().addAll(lineChartPlotters.getInicialSerie(Muscles.ARMS_LEFT), lineChartPlotters.getUpdatedSerie(Muscles.ARMS_LEFT));
		this.lcRightArm.getData().addAll(lineChartPlotters.getInicialSerie(Muscles.ARMS_RIGHT), lineChartPlotters.getUpdatedSerie(Muscles.ARMS_RIGHT));

		//Thigh Measures//	
		this.lcLeftThigh.getData().addAll(lineChartPlotters.getInicialSerie(Muscles.THIGH_LEFT), lineChartPlotters.getUpdatedSerie(Muscles.THIGH_LEFT));
		this.lcRightThigh.getData().addAll(lineChartPlotters.getInicialSerie(Muscles.THIGH_RIGHT), lineChartPlotters.getUpdatedSerie(Muscles.THIGH_RIGHT));

		//Calf Measures//	
		this.lcLeftCalf.getData().addAll(lineChartPlotters.getInicialSerie(Muscles.CALF_LEFT), lineChartPlotters.getUpdatedSerie(Muscles.CALF_LEFT));
		this.lcRightCalf.getData().addAll(lineChartPlotters.getInicialSerie(Muscles.CALF_RIGHT), lineChartPlotters.getUpdatedSerie(Muscles.CALF_RIGHT));
	}

	private void setInicialTfList() {
		inicialTextFieldsHandler.addTextFieldToList(tfWeight, AthleteProfile.ATHLETE_WEIGHT);
		inicialTextFieldsHandler.addTextFieldToList(tfHeight, AthleteProfile.ATHLETE_HEIGHT);
		inicialTextFieldsHandler.addTextFieldToList(tfFat, AthleteProfile.ATHLETE_FAT);
		inicialTextFieldsHandler.addTextFieldToList(tfShouders, Muscles.SHOULDERS);
		inicialTextFieldsHandler.addTextFieldToList(tfChest, Muscles.TORAX);
		inicialTextFieldsHandler.addTextFieldToList(tfWaist, Muscles.WAIST);
		inicialTextFieldsHandler.addTextFieldToList(tfGlutes, Muscles.GLUTES);
		inicialTextFieldsHandler.addTextFieldToList(tfLeftArm, Muscles.ARMS_LEFT);
		inicialTextFieldsHandler.addTextFieldToList(tfRightArm, Muscles.ARMS_RIGHT);
		inicialTextFieldsHandler.addTextFieldToList(tfLefThigh, Muscles.THIGH_LEFT);
		inicialTextFieldsHandler.addTextFieldToList(tfRightThigh, Muscles.THIGH_RIGHT);
		inicialTextFieldsHandler.addTextFieldToList(tfLeftCalf, Muscles.CALF_LEFT);
		inicialTextFieldsHandler.addTextFieldToList(tfRightCalf, Muscles.CALF_RIGHT);

		inicialTextFieldsHandler.setTfEventHandler(floatFilter);
	}

	private void setUpdateslTfList() {
		updateTextFieldsHandler.addTextFieldToList(tfWeightUP, AthleteProfile.ATHLETE_WEIGHT);
		updateTextFieldsHandler.addTextFieldToList(tfHeightUP, AthleteProfile.ATHLETE_HEIGHT);
		updateTextFieldsHandler.addTextFieldToList(tfFatUP, AthleteProfile.ATHLETE_FAT);
		updateTextFieldsHandler.addTextFieldToList(tfShoudersUP, Muscles.SHOULDERS);
		updateTextFieldsHandler.addTextFieldToList(tfChestUP, Muscles.TORAX);
		updateTextFieldsHandler.addTextFieldToList(tfWaistUP, Muscles.WAIST);
		updateTextFieldsHandler.addTextFieldToList(tfGlutesUP, Muscles.GLUTES);
		updateTextFieldsHandler.addTextFieldToList(tfLeftArmUP, Muscles.ARMS_LEFT);
		updateTextFieldsHandler.addTextFieldToList(tfRightArmUP, Muscles.ARMS_RIGHT);
		updateTextFieldsHandler.addTextFieldToList(tfLefThighUP, Muscles.THIGH_LEFT);
		updateTextFieldsHandler.addTextFieldToList(tfRightThighUP, Muscles.THIGH_RIGHT);
		updateTextFieldsHandler.addTextFieldToList(tfLeftCalfUP, Muscles.CALF_LEFT);
		updateTextFieldsHandler.addTextFieldToList(tfRightCalfUP, Muscles.CALF_RIGHT);

		updateTextFieldsHandler.setTfEventHandler(floatFilter);
	}

	private void lineChartClear() {

		this.lcWeight.getData().clear();
		this.lcFat.getData().clear();

		this.lcShoulder.getData().clear();
		this.lcChest.getData().clear();
		this.lcWaist.getData().clear();
		this.lcGlutes.getData().clear();

		this.lcLeftArm.getData().clear();
		this.lcLeftThigh.getData().clear();
		this.lcLeftCalf.getData().clear();

		this.lcRightArm.getData().clear();
		this.lcRightThigh.getData().clear();
		this.lcRightCalf.getData().clear();
	}

}
