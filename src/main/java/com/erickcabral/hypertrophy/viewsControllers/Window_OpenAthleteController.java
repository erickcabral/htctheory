/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.viewsControllers;

import com.erickcabral.hypertrophy.application.Hypertrophy;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import com.erickcabral.hypertrophy.model.AthleteManagement;
import com.erickcabral.hypertrophy.model.AthleteProfile;

/**
 * FXML Controller class
 *
 * @author sexta
 */
public class Window_OpenAthleteController implements Initializable {

	@FXML
	Label lbAthlete;

	@FXML
	TableView<ConvertedAthlete> tableAthleteList;
	@FXML
	TableColumn<ConvertedAthlete, String> nameColumn;
	@FXML
	TableColumn<ConvertedAthlete, String> surnameColumn;
	@FXML
	TableColumn<ConvertedAthlete, String> dateColumn;
	@FXML
	TableColumn<ConvertedAthlete, String> lastUpdateColumn;
	@FXML
	TableColumn<ConvertedAthlete, String> idColumn;
	@FXML
	TableColumn<ConvertedAthlete, String> updatesColumn;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		System.out.println("INICIALIZOU");
		List<AthleteProfile> athleteList = Hypertrophy.getInstance().getSerialization().getAllAthletes();

		ObservableList<ConvertedAthlete> teste = list2ObservableList(athleteList);
		tableAthleteList.setItems(teste);
		idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
		nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		surnameColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));
		dateColumn.setCellValueFactory(new PropertyValueFactory<>("inicialDate"));
		lastUpdateColumn.setCellValueFactory(new PropertyValueFactory<>("lastUpdate"));
		updatesColumn.setCellValueFactory(new PropertyValueFactory<>("totalOfUpdates"));		
	}

	public ObservableList<ConvertedAthlete> list2ObservableList(List<AthleteProfile> list) {
		ObservableList<ConvertedAthlete> convertedList = FXCollections.observableArrayList();

		for (AthleteProfile athleteFinder : list) {
			ConvertedAthlete converted = new ConvertedAthlete();

			int updates = athleteFinder.getMeasuresList().size();
			int lastUpdate = athleteFinder.getMeasuresList().size() - 1;

			converted.setName(athleteFinder.getAthleteName());
			converted.setSurname(athleteFinder.getAthleteSurname());
			converted.setId(String.valueOf(athleteFinder.getAthleteID()));

			converted.setInicialDate(athleteFinder.getMeasuresList().get(0).getProperty(AthleteManagement.DATE_INPUT));
			converted.setTotalOfUpdates(String.valueOf(updates));
			converted.setLastUpdate(athleteFinder.getMeasuresList().get(lastUpdate).getProperty(AthleteManagement.DATE_INPUT));
			
			convertedList.add(converted);
		}
		return convertedList;
	}

	public void close() {
		Hypertrophy.getInstance().getWindowManager().openWindowClose();
	}

	public void open() {
		int id = Integer.valueOf(tableAthleteList.getSelectionModel().getSelectedItem().getId());
		System.out.println("SELECIONADO ID " + id);
		Hypertrophy.getInstance().getSerialization().selectAthleteProfile(id);
		Hypertrophy.getInstance().getWindowManager().openWindowClose();
	}

	protected class ConvertedAthlete {

		String name;
		String surname;
		String id;
		String inicialDate;
		String lastUpdate;
		String totalOfUpdates;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getSurname() {
			return surname;
		}

		public void setSurname(String surname) {
			this.surname = surname;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getInicialDate() {
			return inicialDate;
		}

		public void setInicialDate(String inicialDate) {
			this.inicialDate = inicialDate;
		}

		public String getLastUpdate() {
			return lastUpdate;
		}

		public void setLastUpdate(String lastUpdate) {
			this.lastUpdate = lastUpdate;
		}

		public String getTotalOfUpdates() {
			return totalOfUpdates;
		}

		public void setTotalOfUpdates(String totalOfUpdates) {
			this.totalOfUpdates = totalOfUpdates;
		}
	}

}
