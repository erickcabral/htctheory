/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.viewsControllers;

import com.erickcabral.hypertrophy.application.Hypertrophy;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import com.erickcabral.hypertrophy.model.DayTrainingSetBuilder;
import com.erickcabral.hypertrophy.model.ExerciseParameters;
import com.erickcabral.hypertrophy.model.Muscles;
import com.erickcabral.hypertrophy.supportClasses.PercentagesCalc;
import com.erickcabral.hypertrophy.supportClasses.Plotters;
import com.erickcabral.hypertrophy.supportClasses.TableViewHandler;

/**
 * FXML Controller class
 *
 * @author Erick Cabral
 */
public class Page_DayTrainingBuilderController implements Initializable {

	@FXML
	TableView<Properties> tbvGeneralExerciseList;

	@FXML
	TableView<Properties> tbvDayBuilderExerciseList;

	@FXML
	Button btnAddToDay;
	@FXML
	Button btnRemoveFromDay;
	@FXML
	Button btnSaveDay;
	@FXML
	Button btnClose;

	@FXML
	BarChart<String, Number> barChartMainMuscles;

	@FXML
	PieChart pchartGroups;

	@FXML
	Label lbCaption;

	@FXML
	ChoiceBox<String> chParamFilter;
	@FXML
	ChoiceBox<String> chGroupFilter;
	@FXML
	ChoiceBox<String> chMuscleFilter;

	@FXML
	TextField tfTrainingTag;

	ObservableList<String> paramFilterList = FXCollections.observableArrayList("No Filter");
	ObservableList<String> groupFilterList = FXCollections.observableArrayList("No Filter");
	ObservableList<String> muscleFilterList = FXCollections.observableArrayList("No Filter");

	DayTrainingSetBuilder dayTrainingSet; // Ficar de olho

	//WeekTrainingList weekTrainingList = Hypertrophy.getInstance().getSerialization().getWeekTrainingList();
	ArrayList<Properties> savedExercises = Hypertrophy.getInstance().getSerialization().getAllExercises();
	ObservableList<Properties> generalExerciseList = FXCollections.observableArrayList(savedExercises);
	ObservableList<Properties> dayExerciseList = FXCollections.observableArrayList();

	TableViewHandler generalTableHandler;
	TableViewHandler dayTableHandler;

	Plotters plotter = new Plotters();
	PieChart.Data pieGroupData;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		//this.btnAddToDay.setDisable(true);

		this.tbvDayBuilderExerciseList.setItems(this.dayExerciseList);
		this.tbvGeneralExerciseList.setItems(this.generalExerciseList);
		generalTableHandler = new TableViewHandler(this.tbvGeneralExerciseList);
		this.generalTableHandler.populateBasicInfo("gen", paramFilterList);
		this.generalTableHandler.populateGroup("gen", groupFilterList);
		this.setFilters();

		this.disableWeekList(true);

		this.tbvGeneralExerciseList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Properties>() {
			@Override
			public void changed(ObservableValue<? extends Properties> observable, Properties oldValue, Properties newValue) {
				btnAddToDay.setDisable(newValue == null);
				if (newValue != null) {
					btnAddToDay.setDisable(false);
					System.out.println("CARREGAR GRAFICOS COM ESTE CARA -> " + newValue.getProperty(ExerciseParameters.EXERCISE_NAME));
					plotGroupsPieChart(newValue);
					plotMainMusclesBarChart(newValue);
				} else {
					btnAddToDay.setDisable(true);
				}
			}
		});

		tfTrainingTag.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue.length() > 5) {
					System.out.println("Ja pode Salvar");
					btnSaveDay.setDisable(false);
				} else {
					btnSaveDay.setDisable(true);
				}
			}
		});

		btnAddToDay.setOnAction((event) -> {
			if (this.dayTrainingSet == null) {
				this.dayTrainingSet = new DayTrainingSetBuilder();
			}
			if (this.dayExerciseList == null) {
				System.out.println("está nulo");
			}

			Properties selectedItem = this.tbvGeneralExerciseList.getSelectionModel().getSelectedItem();
			this.tbvDayBuilderExerciseList.getItems().add(selectedItem);

			if (tbvDayBuilderExerciseList.getColumns().isEmpty()) {
				this.dayTableHandler = new TableViewHandler(this.tbvDayBuilderExerciseList);
				this.dayTableHandler.populateBasicInfo("day", paramFilterList);
			}
			this.disableWeekList(false);
			int selectedIndex = this.tbvGeneralExerciseList.getSelectionModel().getSelectedIndex();
			this.tbvGeneralExerciseList.getSelectionModel().clearSelection(); // Limpa Seleção
			this.tbvGeneralExerciseList.getItems().remove(selectedIndex);
		});

		this.btnRemoveFromDay.setOnAction((event) -> {
			Properties selectedItem = this.tbvDayBuilderExerciseList.getSelectionModel().getSelectedItem();  // Trocar depois para "Name" que é o certo
			this.tbvGeneralExerciseList.getItems().add(selectedItem); //ReAdiciona na Lista Principal
			int selectedIndex = this.tbvDayBuilderExerciseList.getSelectionModel().getSelectedIndex();
			this.tbvDayBuilderExerciseList.getItems().remove(selectedIndex);

			if (this.tbvDayBuilderExerciseList.getItems().isEmpty()) { //Desabilita DayList se Vazio.
				this.clearDaySet();
			} else {
				this.plotGroupsPieChart(null);
				this.plotMainMusclesBarChart(null);
			}
		});

		this.btnSaveDay.setOnAction(((event) -> {
			ArrayList<Properties> selectedExercises = new ArrayList<>(this.tbvDayBuilderExerciseList.getItems());
			Properties dayTraningProperties = PercentagesCalc.getListAverages(selectedExercises);
			dayTrainingSet.saveTraining(this.tfTrainingTag.getText(), dayTraningProperties);
		}));

		this.btnClose.setOnAction((event) -> {
			System.out.println("CANCELAR / FECHAR JANELA");
			Hypertrophy.getInstance().getPageManager().closePage();
		});

		this.pchartGroups.getData().addListener(new ListChangeListener<PieChart.Data>() {
			@Override
			public void onChanged(ListChangeListener.Change<? extends PieChart.Data> c) {
				for (Data getter : c.getList()) {
					//System.out.println(String.format("Serie Antes: %s <NODE: %s>", getter.getName(), getter.getNode()));					
					getter.getNode().setId(plotter.getCSSGroupColors(getter.getName()));
					//	System.out.println(String.format("Serie Antes: %s <NODE: %s> : Style: %s", getter.getName(), getter.getNode(), getter.getNode().getStyle()));			 
				}
			}
		});
/*
		this.barChartMainMuscles.getData().addListener(new ListChangeListener<XYChart.Series<String, Number>>() {
			@Override
			public void onChanged(ListChangeListener.Change<? extends XYChart.Series<String, Number>> c) {
				for (XYChart.Series getter : c.getList()) {
					System.out.println("SERIE -> " + getter.getName());
					for(int i =0; i<getter.getData().size(); i++){
						System.out.println("SERIE DATA > " + getter.getData().toString());
						System.out.println("SERIE DATA > " + getter.getData().get(i));					
					}					
				}
			}
		});
*/
	}

	private void disableWeekList(boolean disable) {
		this.tbvDayBuilderExerciseList.setDisable(disable);
		this.tfTrainingTag.clear();
		this.tfTrainingTag.setDisable(disable);
		this.btnRemoveFromDay.setDisable(disable);
	}

	private void populateMainMuscles(TableView tableView, List<String> groupToShow) { //Ajeitar as listas de Grupos em Muscles.
		String sufix = "-";
		if (tableView == tbvGeneralExerciseList) {
			sufix = "colEx";
		} else {
			if (tableView == tbvDayBuilderExerciseList) {
				sufix = "colDay";
			}
		}

		for (String getter : groupToShow) {
			TableColumn<Properties, String> groupColumn = new TableColumn(getter);
			groupColumn.setMinWidth(30d);
			groupColumn.setPrefWidth(50d);
			groupColumn.setId(sufix + getter);
			groupColumn.setCellValueFactory((prop) -> new SimpleStringProperty(String.format("%s %%", prop.getValue().getProperty(getter, "-"))));
			//System.out.println("COL ID -> " + sufix + getter);
			tableView.getColumns().add(groupColumn);
		}
		this.setFilters();
	}

	private Properties selectionFinder(Properties exerciseProperties, ArrayList<Properties> observableList) {
		observableList.remove(exerciseProperties);
		return exerciseProperties;
	}

	private void plotGroupsPieChart(Properties selectedExercise) { //Plota Media dos Grupos trabalhados no SET   
		ArrayList<Properties> trainingSetList = new ArrayList<>(this.tbvDayBuilderExerciseList.getItems());
		this.pchartGroups.getData().clear();
		this.pchartGroups.getData().addAll(plotter.pieChartHotPlot(selectedExercise, trainingSetList));
		this.plotter.showPieChartPercentages(this.pchartGroups);
	}

	private void plotMainMusclesBarChart(Properties selectedExercise) { //Plota Media dos Grupos trabalhados no SET  
		ArrayList<Properties> trainingSetList = new ArrayList<>(this.tbvDayBuilderExerciseList.getItems());
		this.barChartMainMuscles.getData().clear();
		this.barChartMainMuscles.getData().addAll(this.plotter.barChartHotPlot(selectedExercise, trainingSetList));
		this.plotter.addStyleId(this.barChartMainMuscles.getData());		
	}
	
	

	private void setFilters() {
		if (!this.paramFilterList.isEmpty()) {
			chParamFilter.setItems(this.paramFilterList);
			chParamFilter.setValue(this.paramFilterList.get(0));
			chParamFilter.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
				@Override
				public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					exerciseTableFilter(ExerciseParameters.BASIC_INFO_STRING_LIST, newValue.intValue());
				}
			});
		}
		if (!this.groupFilterList.isEmpty()) {
			chGroupFilter.setItems(this.groupFilterList);
			chGroupFilter.setValue(this.groupFilterList.get(0));
			chGroupFilter.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
				@Override
				public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					exerciseTableFilter(Muscles.GROUP_STRING_LIST, newValue.intValue());
				}
			});
		}
		if (!this.muscleFilterList.isEmpty()) {
			chMuscleFilter.setItems(this.muscleFilterList);
			chMuscleFilter.setValue(this.muscleFilterList.get(0));
			chMuscleFilter.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
				@Override
				public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					// TODO
					//exerciseTableFilter(Muscles.STRING_LIST_MAIN_MUSCLES, newValue.intValue());
				}
			});
		}
	}

	private void clearDaySet() {
		if (!this.dayTrainingSet.getExerciseList().isEmpty()) { //Repõe lista Original em caso de cancelamento
			for (Properties getter : this.dayTrainingSet.getExerciseList()) {
				this.generalExerciseList.add(getter);
			}
		}
		this.tbvDayBuilderExerciseList.getItems().clear();
		this.tbvDayBuilderExerciseList.getColumns().clear();
		this.disableWeekList(true);
		this.barChartMainMuscles.getData().clear();
		this.pchartGroups.getData().clear();
		this.dayTrainingSet = null;
	}

	private void exerciseTableFilter(String[] list, int newValue) {
		int index = 0;
		int numberOfColumns = 0;
		int paramColumns = 0;

		if (list == ExerciseParameters.BASIC_INFO_STRING_LIST) {
			paramColumns = 0;
			index = newValue - 1;
			numberOfColumns = ExerciseParameters.BASIC_INFO_STRING_LIST.length;
		} else {
			if (list == Muscles.GROUP_STRING_LIST) {
				paramColumns = ExerciseParameters.BASIC_INFO_STRING_LIST.length;
				index = (newValue - 1) + paramColumns;
				numberOfColumns = Muscles.GROUP_STRING_LIST.length + paramColumns;
			} else {
				System.out.println("ERROR: LIST DOESNT EXIST");
			}
		}

		if (newValue != 0) {
			System.out.println("New Value: " + newValue + "Index: " + index);
			for (int i = paramColumns; i < numberOfColumns; i++) {
				if (i == index) {
					tbvGeneralExerciseList.getColumns().get(i).setVisible(true);
				} else {
					tbvGeneralExerciseList.getColumns().get(i).setVisible(false);
				}
			}
		} else {
			for (int i = paramColumns; i < numberOfColumns; i++) {
				tbvGeneralExerciseList.getColumns().get(i).setVisible(true);
			}
		}
	}
}
