/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.viewsControllers;

import com.erickcabral.hypertrophy.application.Hypertrophy;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import com.erickcabral.hypertrophy.model.AthleteProfile;
import com.erickcabral.hypertrophy.model.Muscles;
import com.erickcabral.hypertrophy.supportClasses.InputEvents;
import com.erickcabral.hypertrophy.supportClasses.TextFieldHandler;

/**
 * FXML Controller class
 *
 * @author sexta
 */
public class Window_AddAthleteController implements Initializable {

	/**
	 * Initializes the controller class.
	 */
	@FXML
	DatePicker datePicker;

	@FXML
	Label lbAthleteID;
	@FXML
	TextField tfName;
	@FXML
	TextField tfSurname;
	@FXML
	TextField tfHeight;
	@FXML
	TextField tfWeight;
	@FXML
	TextField tfFat;
	@FXML
	TextField tfShoulders;
	@FXML
	TextField tfChest;
	@FXML
	TextField tfArm_Left;
	@FXML
	TextField tfArm_Right;
	@FXML
	TextField tfWaist;
	@FXML
	TextField tfGlutes;
	@FXML
	TextField tfThigh_Right;
	@FXML
	TextField tfThigh_Left;
	@FXML
	TextField tfCalf_Right;
	@FXML
	TextField tfCalf_Left;
	@FXML
	Button btnAdd;
	@FXML
	Button btnCancel;

	AthleteProfile athleteProfile = new AthleteProfile();

	EventHandler<KeyEvent> floatFilter = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {
			TextField source = (TextField) event.getSource();
			String inputValue = source.getText();
			if (inputValue.length() > 2 && inputValue.length() <6) {
				if (Float.valueOf(inputValue) == 0f) {
					source.selectAll();
				}
			} else if (inputValue.length() < 6 && !inputValue.isEmpty()) {
				if (!event.getCharacter().equals("\b")) {
					String valid = InputEvents.floatMask(inputValue, 3);
					source.setText(valid);
					source.end();
				}
			} else {
				source.deletePreviousChar();
				source.setText(source.getText());
				source.end();
			}
		}
	};

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		setTextFieldList(); // Inicializa Array de TextFields  
		//this.btnAdd.setDisable(true);
	}

	public void close() {
		Hypertrophy.getInstance().getWindowManager().addWindowClose();
	}

	public void add() {
		if (this.datePicker.getValue() != null) {
			if (this.textFieldHandler.checkBlankTextFields().size() > 0) {
				System.out.println("BLANK FIELDS!");
			} else {
				System.out.println("NO BLANKS");
				Properties newAthleteProps = new Properties();
				String formattedDate = this.datePicker.getValue().format(DateTimeFormatter.ISO_DATE); //Formatted Date
				newAthleteProps.setProperty(AthleteProfile.ATHLETE_NAME, this.tfName.getText());
				newAthleteProps.setProperty(AthleteProfile.ATHLETE_SURNAME, this.tfSurname.getText());
				newAthleteProps.setProperty(AthleteProfile.DATE_INPUT, formattedDate);
				for (TextField getter : this.textFieldHandler.getTextFieldList()) {
					newAthleteProps.setProperty(getter.getId(), getter.getText());
				}
				btnCancel.setText("Close");
				btnAdd.setDisable(true);
				this.athleteProfile.saveAthlete(newAthleteProps);
				//TODO: Save it!

				//----LOG---//
				for (String prop : newAthleteProps.stringPropertyNames()) {
					System.out.println(String.format("PROP ADDED: %s < %s>", prop, newAthleteProps.getProperty(prop)));
				}
			}

		}
	}

	TextFieldHandler textFieldHandler = new TextFieldHandler();

	private void setTextFieldList() {
		textFieldHandler.addTextFieldToList(tfHeight, AthleteProfile.ATHLETE_HEIGHT);
		textFieldHandler.addTextFieldToList(tfWeight, AthleteProfile.ATHLETE_WEIGHT);
		textFieldHandler.addTextFieldToList(tfFat, AthleteProfile.ATHLETE_FAT);
		textFieldHandler.addTextFieldToList(tfShoulders, Muscles.SHOULDERS);
		textFieldHandler.addTextFieldToList(tfChest, Muscles.TORAX);
		textFieldHandler.addTextFieldToList(tfArm_Left, Muscles.ARMS_LEFT);
		textFieldHandler.addTextFieldToList(tfArm_Right, Muscles.ARMS_RIGHT);
		textFieldHandler.addTextFieldToList(tfWaist, Muscles.WAIST);
		textFieldHandler.addTextFieldToList(tfGlutes, Muscles.GLUTES);
		textFieldHandler.addTextFieldToList(tfThigh_Left, Muscles.THIGH_LEFT);
		textFieldHandler.addTextFieldToList(tfThigh_Right, Muscles.THIGH_RIGHT);
		textFieldHandler.addTextFieldToList(tfCalf_Right, Muscles.CALF_LEFT);
		textFieldHandler.addTextFieldToList(tfCalf_Left, Muscles.CALF_RIGHT);

		textFieldHandler.setTfEventHandler(this.floatFilter);
	}
}
