/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.managers;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Erick Cabral
 */
public class PageManager {

	private ScrollPane mainScrollPane;
	private AnchorPane mainAnchorPane;

	public void setMainScrollPane(ScrollPane mainScrollPane) {
		this.mainScrollPane = mainScrollPane;
	}

	public void setMainAnchorPane(AnchorPane mainAnchorPane) {
		this.mainAnchorPane = mainAnchorPane;
	}

	private AnchorPane page;
	private final String PATH = "/fxml/";
	public final String SELECTED_ATHLETE_PAGE = "Page_AthleteProfile.fxml";
	public final String DAY_TRAINING_BUILDER = "Page_DayTrainingBuilder.fxml";
	public final String WEEK_TRAINING_BUILDER = "Page_WeekTrainingBuilder.fxml";

	public void loadDayTrainingBuilderPage() throws IOException {
		this.page = getFXML2Load(this.PATH + this.DAY_TRAINING_BUILDER);
		page.prefWidthProperty().bind(this.mainAnchorPane.widthProperty());
		page.prefHeightProperty().bind(this.mainAnchorPane.heightProperty());
		page.scaleShapeProperty().bind(this.mainAnchorPane.scaleShapeProperty());
		this.mainAnchorPane.getChildren().clear();
		this.mainAnchorPane.getChildren().addAll(page);
	}

	public void loadWeekTrainingBuilderPage() throws IOException {
		this.page = getFXML2Load(this.PATH + this.WEEK_TRAINING_BUILDER);
		page.prefWidthProperty().bind(this.mainAnchorPane.widthProperty());
		page.prefHeightProperty().bind(this.mainAnchorPane.heightProperty());
		page.scaleShapeProperty().bind(this.mainAnchorPane.scaleShapeProperty());
		this.mainAnchorPane.getChildren().clear();
		this.mainAnchorPane.getChildren().addAll(page);
	}

	public void loadAthletePage() throws IOException {
		this.page = getFXML2Load(this.PATH + this.SELECTED_ATHLETE_PAGE);
		page.prefWidthProperty().bind(this.mainAnchorPane.widthProperty());
		page.prefHeightProperty().bind(this.mainAnchorPane.heightProperty());
		page.scaleShapeProperty().bind(this.mainAnchorPane.scaleShapeProperty());
		this.mainAnchorPane.getChildren().clear();
		this.mainAnchorPane.getChildren().addAll(this.page);
	}

	private AnchorPane getFXML2Load(String FXML_Name) throws IOException {
		return FXMLLoader.load(getClass().getResource(FXML_Name));
	}

	public void closePage() {
		if (this.mainScrollPane == null) {
			this.mainAnchorPane.getChildren().clear();
		} else {
			this.mainScrollPane.setContent(null);
		}
	}
}
