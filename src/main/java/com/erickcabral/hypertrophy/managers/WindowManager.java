/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.managers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import com.erickcabral.hypertrophy.viewsControllers.Window_MainProgramController;

/**
 *
 * @author sexta
 */
public class WindowManager {

	public static final String MAIN_PROGRAM = "Window_MainProgram.fxml";
	public static final String MAIN_ADD = "Window_AddAthlete.fxml";
	public static final String MAIN_OPEN = "Window_OpenAthlete.fxml";

	public static final String TRAINING_BUILDER = "TrainingBuilder.fxml";
	public static final String TRAINING_ADD = "Window_TrainingAdd.fxml";
	public static final String TRAINING_OPEN = "Window_TrainingExerciseOpen.fxml";

	private Stage mainStage;

	public Stage getMainStage() {
		return mainStage;
	}

	private WindowMaker mainWindow;
	private Window_MainProgramController mainProgramController;

	private WindowMaker trBuilderWindow;

	private WindowMaker mainAddAthleteWindow;
	private WindowMaker mainOpenWindow;

	private WindowMaker trExerciseAddWindow;
	private WindowMaker trExerciseListWindow;

	/////////////////////////// MAIN WINDOW /////////////////////////  
	public WindowMaker mainWindowShow(String fxml, Stage stage) {
		if (stage == null) {
			stage = mainStage;
		}
		if (mainWindow == null || !mainWindow.isShowing()) {
			this.mainStage = stage;

			mainWindow = new WindowMaker(fxml, stage);
			mainWindow.create();
			mainWindow.setTitle("Hypertrophy Caos Theory");
			// mainWindow.setMaximazed();
		}
		return mainWindow;
	}

	public Window_MainProgramController getMainProgramController() {
		this.mainProgramController = this.mainWindow.getFxmlLoader().getController();
		return mainProgramController;
	}

	public void mainWindowClose() {
		this.mainWindow.close();
		if (mainAddAthleteWindow != null) {
			this.mainAddAthleteWindow.close();
		}
		if (mainOpenWindow != null) {
			this.mainOpenWindow.close();
		}
	}

	public void mainWindowHide() {
		this.mainWindow.hide();
	}
	/////////////////////////// ADD WINDOW /////////////////////////    

	public WindowMaker addAthleteShow(String fxml, Stage stage) {
		if (mainAddAthleteWindow == null || !mainAddAthleteWindow.isShowing()) {
			System.out.println("FIRST INSTANCE OF WINDOW");
			mainAddAthleteWindow = new WindowMaker(fxml, stage);
			mainAddAthleteWindow.create();
			mainAddAthleteWindow.setTitle("Add New Athlete");
		}
		return mainAddAthleteWindow;
	}

	public void addWindowClose() {
		this.mainAddAthleteWindow.close();
	}

	/////////////////////////// OPEN WINDOW /////////////////////////  
	public WindowMaker openWindowShow(String fxml, Stage stage) {
		if (mainOpenWindow == null || !mainOpenWindow.isShowing()) {
			System.out.println("FIRST INSTANCE OF WINDOW");
			mainOpenWindow = new WindowMaker(fxml, stage);
			mainOpenWindow.create();
			mainOpenWindow.setTitle("Open Athlete");
		}
		return mainOpenWindow;
	}

	public void openWindowClose() {
		this.mainOpenWindow.close();
	}

	/////////////////////////// TRAINNING MAIN WINDOW /////////////////////////  
	public WindowMaker trainingMainWindowShow(String fxml, Stage stage) {
		if (stage == null) {
			stage = mainStage;
		}
		if (trBuilderWindow == null || !trBuilderWindow.isShowing()) {
			trBuilderWindow = new WindowMaker(fxml, stage);
			trBuilderWindow.create();
			trBuilderWindow.setTitle("Caos Training Builder");
			
		}
		return trBuilderWindow;
	}

	public void setTrainingBuilderWindowHeight(double height) {
		this.trBuilderWindow.setWindowHeight(height);
	}

	/////////////////////////// TRAINING EXERCISE WINDOW /////////////////////////  
	public WindowMaker trNewExerciseWindowShow(String fxml, Stage stage) {
		if (trExerciseAddWindow == null || !trExerciseAddWindow.isShowing()) {
			trExerciseAddWindow = new WindowMaker(fxml, stage);
			trExerciseAddWindow.create();
			trExerciseAddWindow.setTitle("Set New Exercise");
		}
		return trExerciseAddWindow;
	}

	public void trNewExerciseWindowClose() {
		this.trExerciseAddWindow.close();
	}

}

/////////////////////////// STAGE CREATOR / CONTROLLER /////////////////////////
class WindowMaker {

	//FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
	private final String PATH = "/fxml/";
	private final String FXML_NAME;
	private final Stage stage;
	private FXMLLoader fxmlLoader;

	public WindowMaker(String FXML_NAME, Stage stage) {
		this.FXML_NAME = FXML_NAME;
		this.stage = stage;
	}

	public FXMLLoader getFxmlLoader() {
		return fxmlLoader;
	}

	public void create() {
		Scene scene ;
		try {
			fxmlLoader = new FXMLLoader(getClass().getResource(this.PATH + this.FXML_NAME));
			Parent root = fxmlLoader.load();
			Stage thisStage = this.stage;
			scene = new Scene(root);
			
			//scene.getStylesheets().add("MyTheme.css");
			scene.getStylesheets().add("/styles/MyTheme.css");
			
			thisStage.setScene(scene);
			thisStage.show();

			System.out.println("CREATE WINDOW -> " + this.FXML_NAME);
		}
		catch (Exception e) {
			System.out.println("PATH >" + PATH + FXML_NAME);
			System.out.println(e);
		}
	}

	public void setTitle(String title) {
		this.stage.setTitle(title);
	}

	public void close() {
		this.stage.close();
	}

	public void hide() {
		this.stage.hide();
	}

	public boolean isShowing() {
		return this.stage.isShowing();
	}

	public void setMaximazed() {
		this.stage.setMaximized(true);
	}

	public void setWindowHeight(double height) {
		this.stage.setHeight(height);
	}
	
	public Scene getScene(){
		return this.stage.getScene();
	}

}
