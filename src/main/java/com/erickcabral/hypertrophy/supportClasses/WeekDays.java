/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.supportClasses;

import java.util.Comparator;
import java.util.Properties;
import com.erickcabral.hypertrophy.model.DayTrainingSetBuilder;

/**
 *
 * @author Erick Cabral
 */
public enum WeekDays implements Comparable<WeekDays> {

	Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday;

	public static int getDayIndex(String day) {
		for (WeekDays indexFinder : WeekDays.values()) {
			if (indexFinder.name().equals(day)) {
				return indexFinder.ordinal();
			}
		}
		return -1;
	}

	public static Comparator<String> dayComparatorByString = new Comparator<String>() { //Comparador de Dias!!!!
		@Override
		public int compare(String o1, String o2) {
			if (WeekDays.getDayIndex(o1) > WeekDays.getDayIndex(o2)) {
				return 1;
			} else {
				return -1;
			}
		}
	};

	public static Comparator<WeekDays> dayComparatorByWeekDayClass = new Comparator<WeekDays>() {
		@Override
		public int compare(WeekDays o1, WeekDays o2) {
			if (WeekDays.getDayIndex(o1.toString()) > WeekDays.getDayIndex(o2.toString())) {
				return 1;
			} else {
				return -1;
			}
		}
	};

	public static Comparator<Properties> dayComparatorByProperties = new Comparator<Properties>() {
		@Override
		public int compare(Properties o1, Properties o2) {
			if (WeekDays.getDayIndex(o1.getProperty(DayTrainingSetBuilder.WEEK_DAY)) > WeekDays.getDayIndex(o2.getProperty(DayTrainingSetBuilder.WEEK_DAY))) {
				return 1;
			} else {
				return -1;
			}
		}
	};
}
