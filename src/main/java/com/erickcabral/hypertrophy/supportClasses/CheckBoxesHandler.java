/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.supportClasses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Paint;

/**
 *
 * @author sexta
 */
public class CheckBoxesHandler {

	private final Paint COLOR_CHECKED_FILLED = Paint.valueOf("green");
	private final Paint COLOR_CHECKED_EMPTY = Paint.valueOf("red");
	private final Paint COLOR_UNCHECKED = Paint.valueOf("black");
	private final String SET_ZERO = "";
	private final String SET_HUNDRED = "100";

	private final ObservableList<CheckBoxesSet> checkBoxesSets = FXCollections.observableArrayList();
	private CheckBoxesSet mainGroupBoxSet;

	public CheckBoxesSet getMainGroupBoxSet() {
		return mainGroupBoxSet;
	}

	///////////////// CHECKBOXSET ADD /////////////////////////////////
	public void addCheckBoxSet(CheckBox checkBox, String checkBoxId, TextField textField) {
		this.checkBoxesSets.add(new CheckBoxesSet(checkBox, checkBoxId, textField));
	}
//////////////////----------- Novos Handlers ------------------ //////////////////
	//Seta os EventHandlers dos CheckBoxes

	public void setBoxSetsEventHandler(EventHandler<ActionEvent> selectBoxEvent, EventHandler<KeyEvent> valueFilterEvent) {
		for (CheckBoxesSet getter : this.checkBoxesSets) {
			getter.getCheckBox().setOnAction(selectBoxEvent);
			getter.getTextField().setOnKeyTyped(valueFilterEvent);
		}
	}

	public Properties getGroupProperties() {
		Properties props = new Properties();
		for (CheckBoxesSet propFinder : checkBoxesSets) {
			if (propFinder.getCheckBox().isSelected()) {
				props.setProperty(propFinder.getCheckBox().getId(), propFinder.getTextField().getText());
				//System.out.println(String.format("PROP ADiCIONADO %s -> %s", propFinder.getCheckBox().getId(), propFinder.getTextField().getText()));
			}
		}
		return props;
	}

	public int getGroupValue(String group) {
		return Integer.valueOf(this.checkBoxFinder(group).getTextField().getText());
	}

	public void setMainGroup(String group) {
		for (CheckBoxesSet finder : this.checkBoxesSets) {
			//System.out.println("finder ID: " + finder.getCheckBox().getId() + "<" + group + ">");
			if (finder.getCheckBox().getId().equals(group)) {
				this.mainGroupBoxSet = finder;
				finder.setAsMainGroup(true);
				this.updateBoxSetConfig(group);
			} else {
				finder.setAsMainGroup(false);
				this.updateBoxSetConfig(group);
			}
		}
	}

	public void setSecondaryGroupEnabled(String group, boolean isSelected) {
		if (!isSelected) {
			this.checkBoxFinder(group).getTextField().clear();
		}
		this.boxSetConfig(group, isSelected);
		this.checkBoxFinder(group).getTextField().setDisable(!isSelected);
	}

	public void updateBoxSetConfig(String group) {
		boolean isSelected = checkBoxFinder(group).getCheckBox().isSelected();
		this.boxSetConfig(group, isSelected);
	}

	public void resetBoxConfig() {
		for (CheckBoxesSet finder : this.checkBoxesSets) {
			finder.setDisable();
		}
	}

	private CheckBoxesSet checkBoxFinder(String group) {
		for (CheckBoxesSet finder : this.checkBoxesSets) {
			//System.out.println("finder ID: " + finder.getCheckBox().getId() + "<" + group + ">");
			if (finder.getCheckBox().getId().equals(group)) {
				return finder;
			}
		}
		return null;
	}

	private void boxSetConfig(String group, boolean isSelected) {
		CheckBoxesSet bxSet = this.checkBoxFinder(group);
		if (isSelected) {
			if (bxSet.getTextField().getText().isEmpty()) {
				bxSet.getCheckBox().setTextFill(COLOR_CHECKED_EMPTY);
			} else {
				bxSet.getCheckBox().setTextFill(COLOR_CHECKED_FILLED);
			}
		} else {
			bxSet.getCheckBox().setTextFill(COLOR_UNCHECKED);
		}
	}

	public ArrayList<TextField> getSelecTextFields() {
		ArrayList<TextField> foundTextFields = new ArrayList<>();
		for (CheckBoxesSet finder : this.getSelectedBoxes()) {
			foundTextFields.add(finder.getTextField());
		}
		return foundTextFields;
	}

	public List<CheckBoxesSet> getSelectedBoxes() {
		List<CheckBoxesSet> found = new ArrayList<>();
		for (CheckBoxesSet getter : this.checkBoxesSets) {
			if (getter.getCheckBox().isSelected()) {
				found.add(getter);
			}
		}
		return found;
	}

	//-----------------------------------------------//
	public class CheckBoxesSet implements Serializable {

		private final CheckBox chBoxes;
		private final TextField tfFields;

		public CheckBoxesSet(CheckBox chBoxes, String chBoxId, TextField tfFields) {
			this.chBoxes = chBoxes;
			this.chBoxes.setId(chBoxId);
			this.tfFields = tfFields;
			this.tfFields.setId(chBoxId);
		}

		public CheckBox getCheckBox() {
			return this.chBoxes;
		}

		public TextField getTextField() {
			return this.tfFields;
		}

		public void setDisable() {
			this.chBoxes.setSelected(false);
			this.chBoxes.setDisable(true);
			this.tfFields.setDisable(true);
			this.tfFields.clear();
			this.chBoxes.setTextFill(COLOR_UNCHECKED);
		}

		public void setAsMainGroup(boolean isMainGroup) {
			if (isMainGroup) {
				this.chBoxes.setDisable(true);
				this.chBoxes.setSelected(true);
				this.tfFields.setDisable(true);
				this.tfFields.setText(SET_HUNDRED);
			} else {
				this.chBoxes.setDisable(false);
				this.chBoxes.setSelected(false);
				this.tfFields.setDisable(true);
				this.tfFields.setText(SET_ZERO);
			}
			this.chBoxes.setTextFill(isMainGroup ? COLOR_CHECKED_FILLED : COLOR_UNCHECKED);
		}
	}

}
