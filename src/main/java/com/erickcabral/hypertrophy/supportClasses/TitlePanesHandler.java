/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.supportClasses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Paint;
import com.erickcabral.hypertrophy.model.Muscles;

/**
 *
 * @author sexta
 */
public class TitlePanesHandler {

	String COLOR_DISABLED = "black";
	Paint PAINT_DISABLED = Paint.valueOf(COLOR_DISABLED);

	String COLOR_ENABLED_FILLED = "green";
	Paint PAINT_ENABLED_FULL = Paint.valueOf(COLOR_ENABLED_FILLED);

	String COLOR_ENABLED_EMPTY = "red";
	Paint PAINT_ENABLED_EMPTY = Paint.valueOf(COLOR_ENABLED_EMPTY);

	String SET_ZERO = "";
	String SET_HUNDRED = "100";

	private final ObservableList<TitlePaneSet> titlePaneSets = FXCollections.observableArrayList();
	private final List<TitlePaneSet> exerciseTitlePaneSetList = new ArrayList<>();

	public TitlePanesHandler() {
	}

	public ObservableList<TitlePaneSet> getTpHandlerList() {
		return this.titlePaneSets;
	}

	public List<TitlePaneSet> getExerciseTitledPaneSetList() {
		return this.exerciseTitlePaneSetList;
	}
	///--------------- Novos Handlers ----------------///   

	public void setTitlePaneEventHandlers(EventHandler<KeyEvent> valueFilterEvent) {
		for (TitlePaneSet getter : this.titlePaneSets) {
			for (TextField tf : getter.getTextFieldList()) {
				tf.setOnKeyTyped(valueFilterEvent);
			}
		}
	}

	private TitlePaneSet findPaneSet(String group) {
		for (TitlePaneSet finder : this.titlePaneSets) {
			if (finder.getTitledPane().getId().equals(group)) {
				//System.out.println("TitlePaneSet Found: " + finder.getTitledPane().getId() + "<" + group + ">");
				return finder;
			}
		}
		return null;
	}

	public ArrayList<TextField> getTextFieldsByGroup(String group) {
		return this.findPaneSet(group).getTextFieldList();
	}

	public String getTitlePaneGroupByTextField(String mainMuscle) {
		for (TitlePaneSet finder : this.titlePaneSets) {
			for (TextField tfFinder : finder.getTextFieldList()) {
				if (tfFinder.getId().equals(mainMuscle)) {
					return finder.getTitledPane().getId();
				}
			}
		}
		return null;
	}

	public TextField getTitlePaneTextField(String group, String mainMuscle) {
		for (TextField finder : this.findPaneSet(group).getTextFieldList()) {
			if (finder.getId().equals(mainMuscle)) {
				return finder;
			}
		}
		return null;
	}

	public void setTitlePaneEnable(String group, boolean setEnabled) {
		this.findPaneSet(group).setTitlePaneEnabled(setEnabled);
	}

	public void disableTitlePanes() {
		for (String group : Muscles.STRING_LIST_GROUPS) {
			this.setTitlePaneEnable(group, false);
		}
	}

	public void updateTitlePaneConfig(String group, int totalSum) {
		this.findPaneSet(group).updateTitlePaneConfig(totalSum);
	}

	public void updateTitlePaneBySum(String muscle, String primaryGroup) {
		String group = this.getTitlePaneGroupByTextField(muscle);
		if (!group.equals(primaryGroup)) {
			this.findPaneSet(group).disableTextFieldsBySum();
		}
	}

	public Properties getMuscleProperties() {
		Properties musclesProperties = new Properties();
		for (TitlePaneSet propFinder : this.titlePaneSets) {
			if (!propFinder.getTitledPane().isDisable()) {
				for (TextField valueFinder : propFinder.getTextFieldList()) {
					if (!valueFinder.getText().isEmpty()) {
						musclesProperties.setProperty(valueFinder.getId(), valueFinder.getText());
						//System.out.println(String.format("TITLEPANE PROP ADiCIONADO %s -> %s", valueFinder.getId(), valueFinder.getText()));
					}
				}
			}
		}
		return musclesProperties;
	}

////////---------------------------------------------------////////////////////
	///////////////ADD Title Pane and TextFields //////////////////////////
	public void addTitledPanes(TitledPane titlePane, String IDString) {
		titlePaneSets.add(new TitlePaneSet(titlePane, IDString));
	}

	public void addTpTextField(TitledPane titledPane, TextField tfNew, String ID) {
		for (int i = 0; i < titlePaneSets.size(); i++) {
			if (titlePaneSets.get(i).getTitledPane().equals(titledPane)) {
				titlePaneSets.get(i).addTextField(tfNew, ID);
			}
		}
	}

	public TextField setMainTitlePaneEnabled(String group, String mainMuscle) {
		TitlePaneSet tpSet = this.findPaneSet(group);
		tpSet.setTitlePaneEnabled(true);
		TextField active = tpSet.setMainMuscleActive(mainMuscle);
		tpSet.updateTitlePaneConfig(100);
		return active;
	}

	public void setSecondaryTitlePaneEnabled(String group, boolean enable) {
		TitlePaneSet tpSet = this.findPaneSet(group);
		tpSet.setTitlePaneEnabled(enable);
		tpSet.updateTitlePaneConfig(0);
	}

	/////////////////////////// TextField Manipulators /////////////////////// 
	public void setTextFieldsEnable(String group, String mainMuscle) {
		for (TextField finder : this.getTextFieldListByMuscle(group)) {

			finder.setDisable(false);
		}
	}

	public ArrayList<TextField> getTextFieldListByMuscle(String mainMuscle) {
		for (TitlePaneSet tpFinder : titlePaneSets) {
			//System.out.println("Searching TitlePanes List -> " + tpFinder.getTitledPane().getId() + "<" + mainMuscle + ">");
			for (TextField tfFinder : tpFinder.getTextFieldList()) {
				if (tfFinder.getId().equals(mainMuscle)) {
					return tpFinder.getTextFieldList();
				}
			}
		}
		return null;
	}

//////////////////////////////////////////////////////////// TITLEPANES SETS //////////////////////////////////////
	public class TitlePaneSet implements Serializable {

		private final TitledPane titlePane;

		public TitlePaneSet(TitledPane titlePane, String titlePaneID) {
			this.titlePane = titlePane;
			this.titlePane.setId(titlePaneID);
		}

		public TitledPane getTitledPane() {
			return this.titlePane;
		}

		//////////////// ADICIONANDOP TEXTO /////////////////////////
		ArrayList<TextField> textFieldList = new ArrayList<>();

		public void addTextField(TextField textField, String ID) {
			try {
				if (!ID.equals(" ")) {
					textField.setId(ID);
				}
			}
			catch (Exception e) {
				//System.out.println("ID " + ID);
			}
			textFieldList.add(textField);
		}

		public void removeTextField(TextField textFieldId) {
			for (TextField obj : textFieldList) {
				if (obj.equals(textFieldId)) {
					this.textFieldList.remove(obj);
				}
			}
		}

		public ArrayList<TextField> getTextFieldList() {
			return this.textFieldList;
		}

		public void setTitlePaneEnabled(boolean enable) {
			this.titlePane.setDisable(!enable);
			if (this.titlePane.isDisable()) {
				this.titlePane.setExpanded(false);
			}
			for (TextField finder : this.textFieldList) {
				finder.setDisable(!enable);
				finder.clear();
			}
			this.updateTitlePaneConfig(0);
		}

		public TextField setMainMuscleActive(String mainMuscle) {
			for (TextField finder : this.textFieldList) {
				if (finder.getId().equals(mainMuscle)) {
					finder.setDisable(true);
					finder.setText(SET_HUNDRED);
					return finder;
				}
			}
			return null;
		}

		public void updateTitlePaneConfig(int totalSum) {
			int cont = 0;
			if (!this.titlePane.isDisable()) {
				if (totalSum < 100) {
					this.titlePane.setTextFill(PAINT_ENABLED_EMPTY);
				} else {
					this.titlePane.setTextFill(PAINT_ENABLED_FULL);
				}
			} else {
				this.titlePane.setTextFill(PAINT_DISABLED);
			}
		}

		private boolean disableTextFieldsBySum() {
			int total = 0;
			ArrayList<TextField> emptyTf = new ArrayList<>();
			for (TextField finder : this.textFieldList) {
				if (!finder.getText().isEmpty()) {
					int tfValue = Integer.valueOf(finder.getText());
					total += tfValue;
				} else {
					emptyTf.add(finder);
				}
			}
			if (total == 100) {
				this.titlePane.setTextFill(PAINT_ENABLED_FULL);
				for (TextField getter : emptyTf) {
					getter.setDisable(true);
				}
				return true;
			} else {
				this.titlePane.setTextFill(PAINT_ENABLED_EMPTY);
				for (TextField getter : emptyTf) {
					getter.setDisable(false);
				}
			}
			return false;
		}
	}

}
