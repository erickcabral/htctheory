/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.supportClasses;

import java.util.ArrayList;
import java.util.Properties;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author Erick Cabral
 */
public class TextFieldHandler {

	ArrayList<TextField> textFieldList = new ArrayList<>();

	public void addTextFieldToList(TextField textField, String textField_ID) {
		textField.setId(textField_ID);
		this.textFieldList.add(textField);
	}

	public void setTfEventHandler(EventHandler<KeyEvent> keyEvent) {
		for (TextField getter : this.textFieldList) {
			getter.setOnKeyTyped(keyEvent);
			
		}
	}

	public ArrayList<TextField> checkBlankTextFields() {
		ArrayList<TextField> blankTfs = new ArrayList<>();
		for (TextField finder : this.textFieldList) {
			float value;
			value = finder.getText().isEmpty() ? 0 : Float.valueOf(finder.getText());
			if (value == 0) {
				System.out.println(String.format("INVALID Tf ID: %s <Value: %f>", finder.getId(), value));
				finder.getStyleClass().add("invalidTextField");
				blankTfs.add(finder);
			} else {
				System.out.println(String.format("VALID Tf ID: %s <Value: %f>", finder.getId(), value));
				finder.getStyleClass().add("validTextField");

			}
		}
		return blankTfs;
	}
	
	public ArrayList<TextField> getTextFieldList(){
		return this.textFieldList;
	}
	
	public void setAllTextFieldsDisabled(boolean isDisabled){
		for(TextField getter : this.textFieldList){
			getter.setDisable(isDisabled);
		}
	}
	public void clearTextFields(){
		for(TextField getter: this.textFieldList){
			getter.setText("00.0");
		}
	}
	
	public void writeDataOnTextFields(Properties propToWrite){
		for(TextField finder : this.textFieldList){
			for(String keys : propToWrite.stringPropertyNames()){
				if(finder.getId().equals(keys)){
					finder.setText(propToWrite.getProperty(keys));
					System.out.println(String.format("Key: %s Value: %s", keys,(propToWrite.getProperty(keys))));
				}
			}
		}
	}
}
