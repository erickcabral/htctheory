/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.supportClasses;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Properties;
import com.erickcabral.hypertrophy.model.Muscles;

/**
 *
 * @author Erick Cabral
 */
public class PercentagesCalc {

	public static Properties getListAverages(ArrayList<Properties> trainingSetList) {
		DecimalFormat decimalFormat = new DecimalFormat("#.#");
		Properties trainingAverages = new Properties();
		int listSize = trainingSetList.size();
		//System.out.println("TrainingSetList Size: " + listSize);
		String defaultValue = "0.0";
		for (String group : Muscles.GROUP_STRING_LIST) {
			trainingAverages.setProperty(group, defaultValue);
			//System.out.println(String.format("Prop INITIAL Group Set: %s <%s>", group, value));
			float existentGroupValue = 0f;
			for (Properties getter : trainingSetList) {
				if (getter.containsKey(group)) {
					existentGroupValue = existentGroupValue + Float.valueOf(getter.getProperty(group));
					float groupAverage = existentGroupValue / listSize;
					String newValue = decimalFormat.format(groupAverage).replace(",", ".");
					trainingAverages.put(group, newValue);
					 //System.out.println(String.format("Group<%s> Total<%f> Average<%s>", group, existentGroupValue, newValue));
				}
			}
		}
		//TODO: Rever o calculo para musculos principais com valor acululativo de Stress
		for (String mainMuscles : Muscles.STRING_LIST_MAIN_MUSCLES) {
			trainingAverages.setProperty(mainMuscles, defaultValue);
			//System.out.println(String.format("Prop INITIAL MainMuscle Set: %s <%s>", mainMuscles, value));
			float existentMainMuscleValue = 0f;
			for (Properties getter : trainingSetList) {
				if (getter.containsKey(mainMuscles)) {
					existentMainMuscleValue = existentMainMuscleValue + Float.valueOf(getter.getProperty(mainMuscles));
					float mainMuscleAverage = existentMainMuscleValue / listSize;
					String newValue = decimalFormat.format(mainMuscleAverage).replace(",", ".");
					trainingAverages.put(mainMuscles, newValue);
					//System.out.println(String.format("Main Muscle<%s> Total<%f> Average<%s>", mainMuscles, existentMainMuscleValue, newValue));
				}
			}
		}
		return trainingAverages;
	}
}
