/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.supportClasses;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Properties;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import com.erickcabral.hypertrophy.model.Muscles;
import com.erickcabral.hypertrophy.model.TrainingSetList;
import com.erickcabral.hypertrophy.model.WeekTrainingSetBuilder;

/**
 *
 * @author Erick Cabral
 */
public class FilesHandlers {

	private Stage mainStage;

	public FilesHandlers(Stage mainStage) {
		this.mainStage = mainStage;
	}

	public File createFileToSave(String fileName, String fileType, String directory) { // Excluir esse tipo de arquivo e fazer diferenciado
		FileChooser saveFile = new FileChooser();
		saveFile.setInitialFileName(fileName);
		saveFile.getExtensionFilters().add(getExtensionFilter(fileType));
		saveFile.setInitialDirectory(new File(directory));
		File fileToSave = saveFile.showSaveDialog(this.mainStage);
		return fileToSave;
	}

	private FileChooser.ExtensionFilter getExtensionFilter(String fileType) {
		switch (fileType) {
		case TrainingSetList.FILE_TYPE:
			System.out.println("RETORNOU TRAINING SET");
			return new FileChooser.ExtensionFilter(TrainingSetList.FILE_TYPE, ".hct");
		case WeekTrainingSetBuilder.FILE_TYPE:
			System.out.println("RETORNOU TRAINING SET");
			return new FileChooser.ExtensionFilter(TrainingSetList.FILE_TYPE, ".hcw"); //hypertrophy caos weekList
		default:
			return new FileChooser.ExtensionFilter("ERROR", ".err");
		}
	}

	// ------------- Convertrions XML -> Properties ------------- //
	public static ArrayList<Properties> loadExerciseList() { //String fileLocation) {
		//Teste de Contagem de arquivos - OK
		//File load = new File(fileLocation);
		File load = new File("F:\\Cursos\\JAVA SE\\Projetos\\Hypertrophy\\FILES\\Exercise_File\\");
		File[] listFiles = load.listFiles();
		if (listFiles != null) {
			ArrayList<Properties> exerciseList = new ArrayList<>();
			System.out.println("LOADING SET LIST FILES -> " + listFiles.length);
			for (File getter : listFiles) {
				System.out.println("EXERCISE NAME -> " + getter.getName());
				if (getter.getName().endsWith(".xml")) { //Filtra XML pra naot dar Erro
					Properties convertedXML = loadExerciseParametersFromXML(getter.getName()); //, fileLocation);
					exerciseList.add(convertedXML);
				}
			}
			return exerciseList;
		} else {
			System.out.println("List is NULL on " + load.getPath());
		}
		return null;
	}

	public static Properties loadExerciseParametersFromXML(String exerciseName) {//, String fileLocation) {
		File load = new File("F:\\Cursos\\JAVA SE\\Projetos\\Hypertrophy\\FILES\\Exercise_File\\"); //Tirar HardCode para Configuração do usuario
		File[] listFiles = load.listFiles();
		if (listFiles != null) {
			for (File getter : listFiles) {
				System.out.println("SEARCHING EXERCISE -> " + getter.getName());
				if (getter.getName().startsWith(exerciseName)) {
					Properties exerciseParameters = new Properties();
					try (InputStream loader = new FileInputStream(getter.getAbsolutePath())) {
						exerciseParameters.loadFromXML(loader);
						//System.out.println("PARAMS LOADED -> " + exerciseParameters.getProperty(ExerciseParameters.ID_NAME));
						return exerciseParameters;
					}
					catch (IOException e) {
						System.out.println(e.getStackTrace());
					}
				}
			}
		} else {
			System.out.println("List is NULL on " + load.list());
		}
		return null;
	}

	public static ArrayList<Properties> loadExerciseListParametersFromXML(Properties trainingSetList) {//, String fileLocation) {
		ArrayList<Properties> trainingSetListProperties = new ArrayList<>();
		for (String finder : trainingSetList.stringPropertyNames()) {
			if (finder.startsWith(TrainingSetList.TRAINING_EXERCISE_SUFIX)) {
				String exercise = trainingSetList.getProperty(finder);
				//System.out.println(" FOUND: " + exercise + " LOADED -->" + exerciseFinder(exercise));
				trainingSetListProperties.add(exerciseFinder(exercise));
			}
		}
		if (trainingSetListProperties.isEmpty()) {
			return null;
		}
		return trainingSetListProperties;
	}

	private static Properties exerciseFinder(String exerciseName) { //Funfando
		File load = new File("F:\\Cursos\\JAVA SE\\Projetos\\Hypertrophy\\FILES\\Exercise_File\\"); //Tirar HardCode para Configuração do usuario
		File[] listFiles = load.listFiles();
		if (listFiles != null) {
			for (File getter : listFiles) {
				if (getter.getName().startsWith(exerciseName)) {
					Properties exerciseParameters = new Properties();
					try (InputStream loader = new FileInputStream(getter.getAbsolutePath())) {
						exerciseParameters.loadFromXML(loader);
						//System.out.println("Exercise Found -> " + exerciseName);
						//System.out.println("Exercise Properties Loaded -> " + exerciseParameters.getProperty(ExerciseParameters.ID_NAME));
						return exerciseParameters;
					}
					catch (IOException e) {
						System.out.println(e.getStackTrace());
					}
				}
			}
		} else {
			System.out.println("EXERCISE FILES EMPTY");
		}
		return null;
	}

	public static Properties getAveragesProperties(ArrayList<Properties> exerciseList) {//, boolean plotDetailed) { //Funfando Ok
		//ArrayList<String> listToSeek = plotDetailed ? Muscles.STRING_LIST_MAIN_MUSCLES : Muscles.STRING_LIST_GROUPS;
		DecimalFormat floatFormat = new DecimalFormat("#.#");
		Properties trainingSetAverages = new Properties();

		int divider = exerciseList.size();

		for (String keyFinder : Muscles.STRING_LIST_GROUPS) { //Media dos Grupos
			float total = 0f;
			float average = 0f;
			for (Properties exerciseGetter : exerciseList) {
				if (exerciseGetter.containsKey(keyFinder)) {
					//System.out.println(" GETTER PROPERTIES -> " + exerciseGetter);
					float keyValue = Float.valueOf(exerciseGetter.getProperty(keyFinder));
					total += keyValue;
					average = total / divider;
				}
			}
			if (average > 0) {
				String value = floatFormat.format(average).replace(',', '.');
				System.out.println(String.format("<%s> <%s>", keyFinder, value));
				trainingSetAverages.put(keyFinder, value);
			}
		}

		for (String keyFinder : Muscles.STRING_LIST_MAIN_MUSCLES) { //Media dos Musculos
			float total = 0f;
			float average = 0f;
			for (Properties exerciseGetter : exerciseList) {
				if (exerciseGetter.containsKey(keyFinder)) {
					float keyValue = Float.valueOf(exerciseGetter.getProperty(keyFinder));
					total += keyValue;
					average = total / divider;
				}
			}
			if (average > 0) {
				String value = floatFormat.format(average).replace(',', '.');;
				System.out.println(String.format("<%s> <%s>", keyFinder, value));
				trainingSetAverages.put(keyFinder, value);
			}

		}
		return trainingSetAverages;
	}
}
