/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.supportClasses;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Properties;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import com.erickcabral.hypertrophy.model.Muscles;

/**
 *
 * @author Erick Cabral
 */
public class Plotters extends Muscles {

	//========== PIE CHART GROUP COLORS ==========//
	/*--------------------- Css Colors ---------------------*/
	private final String DEFAULT_CHART_COLOR = "default_chart_color";
	private final String SHO_CSS = "shoulder_style";
	private final String PEC_CSS = "pectoral_style";
	private final String BCK_CSS = "back_style";
	private final String ABS_CSS = "abs_style";
	private final String BIC_CSS = "biceps_style";
	private final String TRI_CSS = "triceps_style";
	private final String GLU_CSS = "glute_style";

	private final String THG_CSS = "thigh_style";
	private final String CAL_CSS = "calves_style";

	public String getCSSGroupColors(String group) {

		switch (group) {
		case Muscles.SHOULDERS:
			return SHO_CSS;
		case Muscles.PECTORALS:
			return PEC_CSS;
		case Muscles.BACK:
			return BCK_CSS;
		case Muscles.ABS:
			return ABS_CSS;
		case Muscles.BICEPS:
			return BIC_CSS;
		case Muscles.TRICEPS:
			return TRI_CSS;
		case Muscles.GLUTES:
			return GLU_CSS;
		case Muscles.THIGHS:
			return THG_CSS;
		case Muscles.CALVES:
			return CAL_CSS;
		default:
			return DEFAULT_CHART_COLOR;
		}
	}

	public String getCSSMainMuscleColors(String mainMuscle) {

		switch (mainMuscle) {
		case SH_TRAPEZIUS:
			return "sho_traps_style";
		case SH_TERES:
		case SH_INFRASPINATUS:
			return "sho_back_style";
		case SH_DELTOID_FRONT:
		case SH_DELTOID_MED:
		case SH_DELTOID_BACK:
			return "sho_delt_style";

		case PEC_UPPER:
			return "pec_upper_style";
		case PEC_MAJOR:
			return "pec_major_style";
		case PEC_LOWER:
			return "pec_lower_style";
		case PEC_INNER:
			return "pec_inner_style";

		case BAK_DORSI:
			return "bck_upper_style";
		case BAK_ERECTOR:
			return "bck_lower_style";

		case BIC_LONG_HEAD:
		case BIC_SHORT_HEAD:
			return BIC_CSS;

		case TRI_INNER_HEAD:
		case TRI_LATERAL_HEAD:
		case TRI_MEDIAL_HEAD:
			return TRI_CSS;

		case ABS_UPPER:
			return "abs_upper_style";
		case ABS_RECTUS:
			return "abs_rectus_style";
		case ABS_LOWER:
			return "abs_lower_style";
		case ABS_OBLIQUE:
			return "abs_oblique_style";

		case GL_MAXIMUS:
		case GL_MEDIUS:
			return GLU_CSS;

		case THG_RETUS_FEMUROUS:
		case THG_VASTUS_LATERALIS:
		case THG_VASTUS_MEDIALIS:
			return "thg_front_style";
		case THG_BICEPS_FEMORIS:
			return "thg_back_style";
		case THG_ABDUCTOR:
			return "thg_outter_style";
		case THG_ADDUCTOR:
			return "thg_inner_style";

		case CAL_GAST_INNER_HEAD:
		case CAL_GAST_LAT_HEAD:
			return CAL_CSS;

		default:
			return DEFAULT_CHART_COLOR;
		}
	}

	/* ----- Legend ----- */
	public void loadLegendItems(Properties averages, HBox legendHbox) {
		legendHbox.getChildren().clear();
		legendHbox.getChildren().addAll(this.getLegend(averages));
		//System.out.println("HBOX CHILDREN -> " + legendHbox.getChildren());		
	}
/*
	private String groupFinder(String muscle) {
		if (MUSCLE_PROPERTIES.containsKey(muscle)) {
			return this.findGroupByMuscle(muscle);
		}
		return null;
	}
*/
	private ArrayList<HBox> getLegend(Properties setListProperties) {
		ArrayList<HBox> legendItems = new ArrayList<>();
		for (String finder : Muscles.STRING_LIST_GROUPS) {
			if (setListProperties.containsKey(finder)) {
				HBox item = this.addLegendItem(finder);
				legendItems.add(item);
				//System.out.println("ADD Legend Item -> " + finder);
			}
		}
		return legendItems;
	}

	private HBox addLegendItem(String groupName) {
		HBox legendBox = new HBox();
		legendBox.setId(groupName);

		legendBox.setAlignment(Pos.CENTER);
		legendBox.setSpacing(10);

		Rectangle symbol = new Rectangle(10, 10);
		//String styleId = this.getGroupStyleId(groupName);
		//symbol.getStyleClass().add(styleId);
		Label groupLabel = new Label(groupName);
		legendBox.getChildren().addAll(symbol, groupLabel);
		//System.out.println(String.format("Legend Item - > %s [CSS: %s]", groupName, styleId));		
		return legendBox;
	}

	private ArrayList<String> getSetListGroups(Properties setListProperties, boolean isDetailed) {
		ArrayList<String> groupList = new ArrayList<>();
		if (isDetailed) {
			for (String getter : setListProperties.stringPropertyNames()) {
				groupList.add(this.findMuscleGroup(getter));
			}
		} else {
			for (String getter : setListProperties.stringPropertyNames()) {
				groupList.add(getter);
			}
		}
		return groupList;
	}

	// --------------------------- Pie Chart data Getter --------------------------------- //
	public ObservableList<PieChart.Data> pieChartHotPlot(Properties selectedExercise, ArrayList<Properties> trainingSetList) {
		ObservableList<PieChart.Data> pieData = FXCollections.observableArrayList();
		DecimalFormat decimalFormat = new DecimalFormat("#.#");

		if (selectedExercise != null) {
			trainingSetList.add(selectedExercise);
		}

		Properties trainingSetListAverages = PercentagesCalc.getListAverages(trainingSetList);

		for (String groupKeyFinder : Muscles.GROUP_STRING_LIST) {
			if (Float.valueOf(trainingSetListAverages.getProperty(groupKeyFinder)) != 0f) {
				float groupAverage = Float.valueOf(trainingSetListAverages.getProperty(groupKeyFinder));
				//	String group = String.format("%s\n%s%%", groupKeyFinder, decimalFormat.format(groupAverage));
				//	PieChart.Data groupData = new PieChart.Data(group, groupAverage);
				PieChart.Data groupData = new PieChart.Data(groupKeyFinder, groupAverage);
				pieData.add(groupData);
			}
		}
		return pieData;
	}

	// --------------------------- Pie Chart Caption Event Handler --------------------------------- //
	public void showPieChartPercentages(PieChart pieChart) {
		DecimalFormat decimalFormat = new DecimalFormat("#.#");
		Label captionLabel = new Label();
		for (final PieChart.Data data : pieChart.getData()) {
			data.getNode().addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					double pieXToScene = data.getNode().getLocalToSceneTransform().getTx();
					double pieYToScene = data.getNode().getLocalToSceneTransform().getTy();
					Pane testee = (Pane) data.getNode().getScene().getRoot().getChildrenUnmodifiable().get(1);
					testee.getChildren().add(captionLabel);
					captionLabel.setLayoutX(pieXToScene);
					captionLabel.setLayoutY(pieYToScene);
					double pieRaioX = event.getX();
					double pieRaioY = event.getY();
					String value = decimalFormat.format(data.getPieValue()).replace(',', '.');
					captionLabel.setText(value + "%");					
					double labelPositionX = pieRaioX;
					double labelPositionY = pieRaioY;
					if(pieRaioX<0){
						labelPositionX = pieRaioX - captionLabel.getWidth();
					}
					if(pieRaioY<0){
						labelPositionY = pieRaioY - (3*captionLabel.getHeight());
					}
					captionLabel.setTranslateX(labelPositionX);
					captionLabel.setTranslateY(labelPositionY);
					captionLabel.toFront();
				}
			});
			data.getNode().addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					Pane testee = (Pane) data.getNode().getScene().getRoot().getChildrenUnmodifiable().get(1);
					testee.getChildren().remove(captionLabel);
				}
			});
		}
	}

	// ----------------------------- BarChart Plotters ----------------------------- //
	public ArrayList<XYChart.Series<String, Number>> barChartHotPlot(Properties selectedExercise, ArrayList<Properties> trainingSetList) {
		ArrayList<XYChart.Series<String, Number>> dataToPlot = new ArrayList<>();
		if (selectedExercise != null) {
			trainingSetList.add(selectedExercise);
		}
		Properties trainingSetListAverages = PercentagesCalc.getListAverages(trainingSetList);
		for (String mainMuscle : Muscles.STRING_LIST_MAIN_MUSCLES) {
			if (Float.valueOf(trainingSetListAverages.getProperty(mainMuscle)) != 0f) {
				float propertyValue = Float.valueOf(trainingSetListAverages.getProperty(mainMuscle));
				XYChart.Data<String, Number> propData = new XYChart.Data<>(mainMuscle, propertyValue);
				//	propSeries.setName(mainMuscle); // COLOCAR O NOME DO GRUPO OU MUSCULO
				//	propSeries.getData().add(new XYChart.Data<>(mainMuscle, propertyValue));
				XYChart.Series<String, Number> propSeries = new XYChart.Series<>();
				propSeries.setName(this.findMuscleGroup(mainMuscle));
				propSeries.getData().add(propData);
				dataToPlot.add(propSeries);
			}
		}
		return dataToPlot;
	}

	public void addStyleId(ObservableList<XYChart.Series<String, Number>> barchartSeries) {
		for (XYChart.Series<String, Number> getter : barchartSeries) {
			for (XYChart.Data<String, Number> finder : getter.getData()) {
				String nodeID = this.getCSSMainMuscleColors(finder.getXValue());
				finder.getNode().setId(nodeID); //Set ID to Node
			}
		}
	}
}
