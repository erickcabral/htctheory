/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.supportClasses;

import java.text.DecimalFormat;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author Erick Cabral
 */
public class InputEvents {

	public static String floatMask(String inputFloat, int numOfDecimals) {
		DecimalFormat floatFormat = new DecimalFormat("##0.0");
		String CLEAR_INPUT = "";
		boolean isDecimal = false;
		if (inputFloat.contains(",") || inputFloat.contains(".")) {
			inputFloat = inputFloat.replace(',', '.');
			isDecimal = true;
		}

		int lastInputIndex = (inputFloat.length() > 0) ? inputFloat.length() - 1 : 0;
		char lastChar = inputFloat.charAt(lastInputIndex);

		String lastString = inputFloat.substring(0, lastInputIndex);

		if (inputFloat.length() > 4 && inputFloat.contains(".")) {
			inputFloat = lastString;
		} else if (!Character.isDigit(lastChar) && lastChar != '.') {
			inputFloat = inputFloat.length() > 0 ? inputFloat.substring(0, lastInputIndex) : CLEAR_INPUT;
		} else if (inputFloat.length() == 1 && lastChar == '.') {
			inputFloat = "0.";
			return inputFloat;
		} else if (lastString.contains(".")) {
			if (lastChar == '.') {
				return lastString;
			}
		} else if (lastChar == '.') {
			return inputFloat;
		}

		//System.out.println(String.format("Last CHAR: %c <Index: %d>", lastChar, lastInputIndex));
		if (inputFloat.length() > numOfDecimals) {
			Float valueToFloat;
			if (isDecimal) {
				if (lastChar == '.') {
					return inputFloat;
				} else {
					valueToFloat = Float.valueOf(inputFloat);
					inputFloat = floatFormat.format(valueToFloat).replace(",", ".");
				}
			} else {
				valueToFloat = Float.valueOf(inputFloat) / 10;
				inputFloat = floatFormat.format(valueToFloat).replace(",", ".");
			}
		} else {
			float valueToFloat;
			if (!inputFloat.contains(".")) { //is Decimal	
				return inputFloat;
			} else {
				valueToFloat = Float.valueOf(inputFloat);
				inputFloat = floatFormat.format(valueToFloat).replace(',', '.');
			}
		}
		return inputFloat;
	}

	public static String inputFloatFilter(int maxValue, String inputString, int maxInputLength) {
		DecimalFormat floatFormat = new DecimalFormat("#00.0");
		String valueInput = inputString;
		int valueInputLength = valueInput.length();
		int lastInputIndex = valueInputLength - 1;
		int integerLength = maxInputLength - 2;

		char lastChar = valueInput.charAt(lastInputIndex);
		if (valueInputLength > maxInputLength) {
			valueInput = valueInput.subSequence(0, maxInputLength).toString();
		} else {
			if (!Character.isDigit(lastChar)) { //Non Digit Filter
				if (lastChar == ',') {
					if (valueInput.contains(".")) { //is Decimal
						valueInput = valueInput.subSequence(0, lastInputIndex).toString();
					} else { //is not Decimal
						valueInput = valueInput.replace(',', '.');
					}
				} else {
					valueInput = valueInput.subSequence(0, lastInputIndex).toString();
				}
			} else {
				float valueToFloat;
				if (valueInput.contains(".")) { //is Decimal   
					valueToFloat = Float.valueOf(valueInput);
					valueInput = floatFormat.format(valueToFloat).replace(",", ".");
				} else {
					if (valueInputLength > integerLength) {
						valueToFloat = Float.valueOf(valueInput) / 10;
						valueInput = floatFormat.format(valueToFloat).replace(',', '.');
					}
					if (Float.valueOf(valueInput) >= maxValue) {
						valueInput = floatFormat.format(maxValue).replace(',', '.');
					}
				}
			}
		}
		return valueInput;
	}

	public boolean isIntFilterValid(boolean isGroup, KeyEvent event) {
		TextField tf = (TextField) event.getSource();
		String inputString = tf.getText();
		int textFieldLength = inputString.length();
		char valueInput = event.getCharacter().charAt(0);

		if (valueInput == '\b' || valueInput == '\r') {
			return true;
		}
		if (!Character.isDigit(valueInput)) {
			tf.deletePreviousChar();
		} else {
			if (textFieldLength > 2) {
				if (!inputString.startsWith("100")) {
					tf.deletePreviousChar();
				}
			}
			return true;
		}
		return false;
	}

}
