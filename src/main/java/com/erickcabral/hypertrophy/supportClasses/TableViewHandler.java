/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.supportClasses;

import java.util.Properties;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import com.erickcabral.hypertrophy.model.ExerciseParameters;
import com.erickcabral.hypertrophy.model.Muscles;

/**
 *
 * @author Erick Cabral
 */
public class TableViewHandler {

	TableView tableView;

	public TableViewHandler(TableView tableView) {
		this.tableView = tableView;
	}

	public void populateBasicInfo(String id_sufix, ObservableList<String> paramFilterList) {
		System.out.println("TABLE VIEW >> " + tableView.toString());
		for (String getter : ExerciseParameters.BASIC_INFO_STRING_LIST) {
			if ((paramFilterList.size() - 1) < ExerciseParameters.BASIC_INFO_STRING_LIST.length) {
				paramFilterList.add(getter);
				System.out.println("PARALIST > " + getter);
			}
			TableColumn<Properties, String> groupColumn = new TableColumn(getter);
			groupColumn.setMinWidth(30d);
			groupColumn.setPrefWidth(50d);
			groupColumn.setId(id_sufix + getter);
			groupColumn.setCellValueFactory((prop) -> new SimpleStringProperty(prop.getValue().getProperty(getter, "-")));
			//System.out.println("COL ID -> " + sufix + getter);
			this.tableView.getColumns().add(groupColumn);
		}
	}

	public void populateGroup(String id_sufix, ObservableList<String> groupFilterList) {
		for (String getter : Muscles.STRING_LIST_GROUPS) {
			if ((groupFilterList.size() - 1) < Muscles.STRING_LIST_GROUPS.size()) {
				groupFilterList.add(getter);
				System.out.println("GROUPLIST > " + getter);
			}
			TableColumn<Properties, String> groupColumn = new TableColumn(getter);
			groupColumn.setMinWidth(30d);
			groupColumn.setPrefWidth(50d);
			groupColumn.setId(id_sufix + getter);
			groupColumn.setCellValueFactory((prop) -> new SimpleStringProperty(String.format("%s %%", prop.getValue().getProperty(getter, "-"))));
			//System.out.println("COL ID -> " + sufix + getter);
			tableView.getColumns().add(groupColumn);
		}
	}
}
