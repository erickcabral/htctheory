/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.supportClasses;

import java.util.Properties;
import javafx.scene.chart.XYChart;
import com.erickcabral.hypertrophy.model.AthleteManagement;
import com.erickcabral.hypertrophy.model.AthleteProfile;

/**
 *
 * @author sexta
 */
public class LineChartPlotters {

	private String dataToPlot;
	private String leftData;
	private String rightData;
	private final AthleteProfile profile;

	public String getDataToPlot() {
		return dataToPlot;
	}

	public String getLeftData() {
		return leftData;
	}

	public String getRightData() {
		return rightData;
	}

	public LineChartPlotters(AthleteProfile profile) {
		this.profile = profile;
	}

	public LineChartPlotters(String dataToPlot, AthleteProfile profile) {
		this.dataToPlot = dataToPlot;
		this.profile = profile;
	}

	public LineChartPlotters(String leftData, String rightData, AthleteProfile profile) {
		this.leftData = leftData;
		this.rightData = rightData;
		this.profile = profile;
	}
	
	public XYChart.Series<String, Float> getInicialSerie(String dataToPLot) {
		String initialValue = profile.getMeasuresList().get(0).getProperty(dataToPLot);
		String initialDate = profile.getMeasuresList().get(0).getProperty(AthleteProfile.DATE_INPUT);
		System.out.println(String.format("Getting Inicial Muscle Data: %s < %s > - %s", dataToPLot, initialValue, initialDate));

		XYChart.Series<String, Float> inicialSerie = new XYChart.Series<>();
		inicialSerie.setName("Initial Date");

		for (Properties getter : profile.getMeasuresList()) {
			String date = getter.getProperty(AthleteManagement.DATE_INPUT);
			inicialSerie.getData().add(new XYChart.Data<>(date, Float.valueOf(initialValue)));
		}
		return inicialSerie;
	}

	public XYChart.Series<String, Float> getUpdatedSerie(String dataToPLot) {
		XYChart.Series<String, Float> updatedSerie = new XYChart.Series<>();
		updatedSerie.setName("Development");

		for (Properties getter : profile.getMeasuresList()) {
			String date = getter.getProperty(AthleteProfile.DATE_INPUT);
			String value = getter.getProperty(dataToPLot);
			System.out.println(String.format("Getting Updates Muscle Data: %s < %s > - %s", dataToPLot, value, date));
			updatedSerie.getData().add(new XYChart.Data<>(date, Float.valueOf(value)));
		}
		return updatedSerie;
	}
}
