/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import com.erickcabral.hypertrophy.model.AthleteProfile;
import com.erickcabral.hypertrophy.model.DayTrainingSetBuilder;
import com.erickcabral.hypertrophy.model.ExerciseParameters;
import com.erickcabral.hypertrophy.model.WeekTrainingSetBuilder;
import com.erickcabral.hypertrophy.supportClasses.FilesHandlers;

/**
 *
 * @author Erick
 */
public class Serialization {

	private final String ATHLETE_LIST = "F:\\Cursos\\JAVA SE\\Projetos\\Hypertrophy\\FILES\\Athelete List\\Athletes.tch";
	private final String EXERCISE_FILE = "F:\\Cursos\\JAVA SE\\Projetos\\Hypertrophy\\FILES\\Exercise_File\\";
	private final String TRAINING_FILE = "F:\\Cursos\\JAVA SE\\Projetos\\Hypertrophy\\FILES\\Week List\\WeekList.sch";

	public Serialization() {
		this.getAllExercises();
		openAthleteFile();
		openWeekList();
	}

	///////////////////////////////////////////// EXERCISE SAVE ////////////////////////////////////////////// 
	private ArrayList<Properties> exerciseList;

	public boolean saveExercise(Properties exerciseProperties) {
		return this.persistExerecise(exerciseProperties);
	}

	/*
	private boolean persistExereciseList() {
		try (ObjectOutputStream oas = new ObjectOutputStream(new FileOutputStream(EXERCISE_LIST))) {
			oas.writeObject(this.exerciseList);
			System.out.println("Lista de Exercícios Salvo com Sucesso!");
			for (int i = 0; i < this.exerciseList.size(); i++) {
				System.out.println(String.format("SAVED EXERCISES: %s", this.exerciseList.get(i).getProperty(ExerciseParameters.EXERCISE_NAME)));
			}
			return true;
		}
		catch (IOException e) {
			throw new RuntimeException("Arquivo nÃ£o foi salvo/Criado! ", e);
		}
	}
	 */
	private boolean persistExerecise(Properties exerciseProperties) {
		String exerciseName = exerciseProperties.getProperty(ExerciseParameters.EXERCISE_NAME);
		String fileName = String.format("%s.xml", exerciseName);
		String FILE_PATH = EXERCISE_FILE + fileName;
		try (OutputStream fos = new FileOutputStream(FILE_PATH)) {
			exerciseProperties.storeToXML(fos, "Storing XML");
			System.out.println("Arquivo XML de Exercícios Salvo com Sucesso! -> " + exerciseProperties);
			FilesHandlers.loadExerciseList();
			return true;
		}
		catch (IOException e) {
			throw new RuntimeException("Arquivo não foi salvo/Criado! ", e);
		}
	}

	public Properties loadExerciseParametersFromXML(String exerciseName) {
		return FilesHandlers.loadExerciseParametersFromXML(exerciseName);
	}

	public ArrayList<Properties> getAllExercises() {
		if (this.exerciseList == null) {
			this.exerciseList = new ArrayList<>();
		}
		this.exerciseList = FilesHandlers.loadExerciseList();
		return this.exerciseList;
	}

	///////////////////////////////////////////// ATHLETE SAVE //////////////////////////////////////////////
	private List<AthleteProfile> athleteList;
	private int id;

	private int addAthleteID() {
		int lastID;
		System.out.println("LIST SIZE >> " + athleteList.size());
		if (this.athleteList.size() == 0) {
			lastID = 1;
		} else {
			int lastposition = this.athleteList.size() - 1;
			lastID = this.athleteList.get(lastposition).getAthleteID() + 1;
		}
		return lastID;
	}

	public boolean saveAthlete(AthleteProfile profile) { //Revisar metodo de adiçção
		if (profile.getAthleteID() == 0) {
			profile.setAthleteID(addAthleteID());
			profile.getMeasuresList().get(0).setProperty(AthleteProfile.ATHLETE_ID, String.valueOf(addAthleteID()));
			System.out.println("ADDING NEW ID > " + profile.getAthleteID());
			this.athleteList.add(profile);
		} else {
			int position = profile.getAthleteID() - 1;
			System.out.println("SERIALIZING - REMOVENDO POSITION " + position);
			this.athleteList.remove(position);
			System.out.println("\"SERIALIZING - ADICIONANDO POSITION " + position + " ATHLETA ID " + profile.getAthleteID());
			this.athleteList.add(position, profile);
		}

		return persistAthlete(); //Retorna o resultado do salvamento.
	}

	public List<AthleteProfile> getAllAthletes() {
		return new ArrayList<>(this.athleteList);
	}

	public void selectAthleteProfile(int id) {
		for (AthleteProfile finder : this.athleteList) {
			if (finder.getAthleteID() == id) {
				Hypertrophy.getInstance().setSelectedProfile(finder);
				Hypertrophy.getInstance().getWindowManager().getMainProgramController().loadAthletePage();
				System.out.println(String.format("Selected Profile NAME > %s", finder.getAthleteName()));
				break;
			}
		}
	}

	private boolean persistAthlete() {
		try (ObjectOutputStream oas = new ObjectOutputStream(new FileOutputStream(ATHLETE_LIST))) {
			oas.writeObject(this.athleteList);
			System.out.println("Athleta Salvo com Sucesso!");
			for (int i = 0; i < this.athleteList.size(); i++) {
				System.out.println("NAME >>>> " + this.athleteList.get(i).getAthleteName());
			}
			return true;
		}
		catch (IOException e) {
			throw new RuntimeException("Erro ao Salvar", e);
		}
	}

	private void openAthleteFile() {
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(ATHLETE_LIST))) {
			athleteList = (List<AthleteProfile>) ois.readObject();
			System.out.println("Lista de Athleta Carregada com Sucesso!");
			for (int i = 0; i < this.athleteList.size(); i++) {
				System.out.println("ID's >> " + athleteList.get(i).getAthleteID());
			}
		}
		catch (ClassNotFoundException | IOException e) {
			System.out.println("Not Loaded: Lista de Athleta doesn't exist!");
			athleteList = new ArrayList<>();
			persistAthlete();
			System.out.println("INITIALIZING Lista de Athleta");
		}
	}

	///////////////////////////////////////////// TRAINING SAVE ////////////////////////////////////////////// 
	private DayTrainingSetBuilder[] dayTraining;

	public ArrayList<DayTrainingSetBuilder> getWeekTrainingList() {
		ArrayList<DayTrainingSetBuilder> daySets = new ArrayList<>();
		openWeekList();
		for (DayTrainingSetBuilder finder : dayTraining) {
			if (finder != null) {
				daySets.add(finder);
			}
		}
		return daySets;
	}

	public boolean saveDayTrainingSet(DayTrainingSetBuilder daySet, int dayInt) {
		this.dayTraining[dayInt] = daySet;
		return persistWeekList(); //Retorna o resultado do salvamento.
	}

	private boolean persistWeekList() {
		try (ObjectOutputStream oas = new ObjectOutputStream(new FileOutputStream(TRAINING_FILE))) {
			oas.writeObject(this.weekTrainingList);
			System.out.println("Arquivo de Treino Salvo com Sucesso!");

			for (DayTrainingSetBuilder finder : this.dayTraining) {
				if (finder != null) {
					//System.out.println(String.format("Treino Salvo: %s, dia: %s", finder.getTrainingTagName(), finder.getDayString()));
				}
			}
			return true;
		}
		catch (IOException e) {
			throw new RuntimeException("Arquivo Treino nao salvo/Criado! ", e);
		}
	}

	private void openWeekList() {
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(TRAINING_FILE))) {
			this.dayTraining = (DayTrainingSetBuilder[]) ois.readObject();
			System.out.println("Arquivo de Treino Lido com Sucesso!");
			for (DayTrainingSetBuilder finder : this.dayTraining) {
				if (finder != null) {
					//System.out.println(String.format("Treino: %s, dia: %s", finder.getTrainingTagName(), finder.getDayString()));
				}
			}
		}
		catch (ClassNotFoundException | IOException e) {
			System.out.println("Not Loaded: TRAINING File doesn't exist!");
			this.dayTraining = new DayTrainingSetBuilder[7];
			System.out.println("INITIALIZING TRAININGLIST");
		}
	}

	/* ------------------------------ TRAINING SET SAVING  ------------------------------ */
	//private Properties trainingSet;
	private ArrayList<DayTrainingSetBuilder> dayTrainingSetBuildersList;

	private DayTrainingSetBuilder trainingSetBuilder;

	public boolean saveDaySetList(DayTrainingSetBuilder trainingSetBuilder, File fileInfo) {
		this.trainingSetBuilder = trainingSetBuilder;
		return persistSetList(fileInfo); //Retorna o resultado do salvamento.
	}

	public ArrayList<DayTrainingSetBuilder> getTrainingSetList() {
		if (this.dayTrainingSetBuildersList == null) {
			this.loadTrainingLists();
		}
		return this.dayTrainingSetBuildersList;
	}

	private boolean persistSetList(File fileInfo) {
		try (ObjectOutputStream oas = new ObjectOutputStream(new FileOutputStream(fileInfo))) {
			oas.writeObject(this.trainingSetBuilder);
			System.out.println("Arquivo de Treino Salvo com Sucesso!");
			System.out.println("Training Properties -> " + this.trainingSetBuilder);
			System.out.println("FILE PATH -> " + fileInfo.getAbsolutePath());
			System.out.println("FILE NAME -> " + fileInfo.getName());
			System.out.println("FILE -> " + fileInfo.toString());
			oas.close();
			loadTrainingLists();
			return true;
		}
		catch (IOException e) {
			throw new RuntimeException("Arquivo Treino nao salvo/Criado! ", e);
		}
	}

	private ArrayList<DayTrainingSetBuilder> loadTrainingLists() {
		this.dayTrainingSetBuildersList = new ArrayList<>();
		File load = new File(DayTrainingSetBuilder.DAYSET_DEFAULT_FOLDER); //Corrigir caninho após criar Arquivo de Config do APP
		File[] listFiles = load.listFiles();
		if (listFiles != null) {
			for (File getter : listFiles) {
				if (getter.getAbsoluteFile().getName().endsWith(".hct")) {
					System.out.println("TRAININGSET FILE NAME -> " + getter.getName());
					try (ObjectInputStream loader = new ObjectInputStream(new FileInputStream(getter.getAbsolutePath()))) {
						DayTrainingSetBuilder loadTrainingSets = (DayTrainingSetBuilder) loader.readObject();
						this.dayTrainingSetBuildersList.add(loadTrainingSets);
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			System.out.println("List is NULL on " + load.list());
		}
		return this.dayTrainingSetBuildersList;
	}

	/* ------------------------------ WEEK SET SAVING  ------------------------------ */
/*	private ArrayList<Properties> weekSet;
	private ArrayList<ArrayList<Properties>> weekSetList;

	private WeekTrainingSetBuilder weekTrainingSetBuilder;


	/*
	public boolean saveWeekSetList(ArrayList<Properties> weekSetListProperties, File fileInfo) {
		this.weekSet = weekSetListProperties;
		return persistWeekSetList(fileInfo); //Retorna o resultado do salvamento.
	}

	private boolean persistWeekSetList(File fileInfo) {
		try (ObjectOutputStream oas = new ObjectOutputStream(new FileOutputStream(fileInfo))) {
			oas.writeObject(this.weekSet);
			oas.close();
			loadWeekTrainingLists();
			System.out.println("Arquivo de Treino Salvo com Sucesso!");
			for (Properties saved : this.weekSet) {
				System.out.println("Arquivos salvos -> " + saved.getProperty("Week Day"));
				System.out.println("Arquivos salvos -> " + saved.getProperty("Training Tag"));
			}
			return true;
		}
		catch (IOException e) {
			throw new RuntimeException("Arquivo Treino nao salvo/Criado! ", e);
		}
	}

	public ArrayList<ArrayList<Properties>> loadWeekTrainingLists() {
		this.weekSetList = new ArrayList<>();

		File load = new File(this.WEEK_SETLIST);
		File[] listFiles = load.listFiles();
		if (listFiles != null) {
			System.out.println("TRAINING FILES -> " + listFiles.length);
			for (File getter : listFiles) {
				System.out.println("TRAINING FILE NAME -> " + getter.getName());
				try (ObjectInputStream loader = new ObjectInputStream(new FileInputStream(getter.getAbsolutePath()))) {
					ArrayList<Properties> loadProperties = (ArrayList<Properties>) loader.readObject();
					this.weekSetList.add(loadProperties);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			System.out.println("List is NULL on " + load.list());
		}
		return this.weekSetList;
	}

	public ArrayList<ArrayList<Properties>> getWeekTrainingSetList() {
		if (this.weekSet == null) {
			return this.loadWeekTrainingLists();
		}
		return this.weekSetList;
	}
	 */
	
	private String WEEK_SETLIST = "C:\\Users\\Erick Cabral\\Desktop\\WEEK SAVING";
	private ArrayList<WeekTrainingSetBuilder> weekTrainingList = new ArrayList<>();
	
	public boolean saveWeekSetList(WeekTrainingSetBuilder weekTrainingSet, File fileInfo) {
		return persistWeekSetList(weekTrainingSet, fileInfo); //Retorna o resultado do salvamento.
	}
	
	private boolean persistWeekSetList(WeekTrainingSetBuilder weekTrainingSet, File fileInfo) {
		try (ObjectOutputStream oas = new ObjectOutputStream(new FileOutputStream(fileInfo))) {
			oas.writeObject(weekTrainingSet);
			oas.close();
			loadWeekTrainingLists();
			System.out.println("Arquivo de Treino Salvo com Sucesso!");
			System.out.println("Week Saved: " + weekTrainingSet.getWeekDay() +  ">" + weekTrainingSet.getWeekTrainingTag());
			return true;
		}
		catch (IOException e) {
			throw new RuntimeException("Arquivo Treino nao salvo/Criado! ", e);
		}
	}

	public ArrayList<WeekTrainingSetBuilder> loadWeekTrainingLists() {
		File load = new File(this.WEEK_SETLIST);
		File[] listFiles = load.listFiles();
		if (listFiles != null) {
			System.out.println("TRAINING FILES -> " + listFiles.length);
			for (File getter : listFiles) {
				System.out.println("TRAINING FILE NAME -> " + getter.getName());
				if (getter.getName().endsWith(".hcw")) {
					try (ObjectInputStream loader = new ObjectInputStream(new FileInputStream(getter.getAbsolutePath()))) {
						WeekTrainingSetBuilder weekTrainingSaved = (WeekTrainingSetBuilder) loader.readObject();
						weekTrainingList.add(weekTrainingSaved);
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			System.out.println("List is NULL on " + load.list());
		}
		return weekTrainingList;
	}

	public ArrayList<WeekTrainingSetBuilder> getWeekTrainingSetList() {
		if (this.weekTrainingList == null) {
			return this.loadWeekTrainingLists();
		}
		return this.weekTrainingList;
	}

}
