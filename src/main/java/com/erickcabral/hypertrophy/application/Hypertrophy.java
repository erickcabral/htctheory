/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.application;

import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;
import com.erickcabral.hypertrophy.managers.PageManager;
import com.erickcabral.hypertrophy.managers.WindowManager;
import com.erickcabral.hypertrophy.model.AthleteProfile;
import com.erickcabral.hypertrophy.model.Calculations;

/**
 *
 * @author sexta
 */
public class Hypertrophy extends Application {

	private static Hypertrophy instance; // Cria Variavel para inasancia
	private WindowManager windowManager; // Criando instancia de WindowManager
	private Calculations calculations; // Criando instancia de Calculations
	private AthleteProfile selectedProfile; // Criando instancia de Serializaçao
	private Serialization serialization; // Criando instancia de Serializaçao
	private PageManager pageManager; // Criando instancia de Serializaçao
	private FireApi fireApi; // Criando instancia de FireBase
	
	//  private TrainingBuilderController_ExerciseListScreen trBuilderMainController_listScreen; // Criando ....
	//  private TrainingBuilderController_WeekScreen trBuilderMainController_WeekScreen; // Criando ....     
	//  private WeekTrainingList weekTrainingList;

///////// Inicializa a Classe ///////////
	public Hypertrophy() {
		if (instance == null) { // Se Não existe instancia
			instance = this;
			this.windowManager = new WindowManager();
			this.calculations = new Calculations();
			this.serialization = new Serialization();
			this.pageManager = new PageManager();
			this.fireApi = new FireApi();
		}
	}

	//////////// getters das Instancias //////////////   
	public static Hypertrophy getInstance() {
		return instance;
	}

	public AthleteProfile getSelectedProfile() {
		return selectedProfile;
	}

	public void setSelectedProfile(AthleteProfile selectedProfile) {
		this.selectedProfile = selectedProfile;
	}

	public WindowManager getWindowManager() {
		return this.windowManager;
	}

	public Calculations getCalculations() {
		return this.calculations;
	}

	public Serialization getSerialization() {
		return this.serialization;
	}

	public PageManager getPageManager() {
		return pageManager;
	}

	
	

	
	

//////////// Start do Programa ////////////////
	@Override
	public void start(Stage primaryStage) throws IOException {
		this.windowManager.mainWindowShow(WindowManager.MAIN_PROGRAM, primaryStage);  /// Cria e mostra Janela
	}

	public static void main(String[] args) {
		launch(args);
	}
}
