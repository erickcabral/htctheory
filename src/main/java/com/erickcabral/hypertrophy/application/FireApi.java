/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.application;


import com.firebase.client.Firebase;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 * @author Erick Cabral
 */
public class FireApi {

	private final String key = "F:\\Cursos\\JAVA SE\\Bibliotecas\\Chave Firebase HCT\\FireCredential.json";
	private final String firebaseURL = "https://hypercaostheory.firebaseio.com/";

	FileInputStream serviceAccount;
	FirebaseOptions options;
	GoogleCredentials credential;
	Firebase firebase;
	
	public FireApi() {	
		System.out.println("++++++ FIREBASE ++++++");
		try {
			serviceAccount = new FileInputStream(key);
			credential = GoogleCredentials.fromStream(serviceAccount);
			System.out.println("CRED -> " + credential);
			options = FirebaseOptions.builder().setCredentials(credential)
				.setDatabaseUrl("https://hypercaostheory.firebaseio.com")
				.build();

			FirebaseApp.initializeApp(options);
			firebase = new Firebase("https://hypercaostheory.firebaseio.com");
			System.out.println(" FIRE -->> " + firebase);
			firebase.child("NAME").setValue("NEW TESTE DONE NETBEANS!");
			firebase.getApp().goOffline();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
/*
	public void createApi(){
		System.out.println("++++++ FIREBASE ++++++");
		try {
			serviceAccount = new FileInputStream(key);
			credential = GoogleCredentials.fromStream(serviceAccount);
			firebase = new Firebase(firebaseURL);
			System.out.println(" FIRE -->> " + firebase);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	*/
}
