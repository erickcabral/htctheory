/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Properties;

/**
 *
 * @author Erick Cabral
 */
public class TrainingSetList implements Serializable {

	public final static String FILE_TYPE = "Training List";
	public final static String TRAINING_TAG = "Training Tag";
	public final static String TRAINING_EXERCISE_SUFIX = "Exercise";

	String trainingtag;
	ArrayList<Properties> exerciseList;

	public TrainingSetList(String trainingtag, ArrayList<Properties> exerciseList) {
		this.trainingtag = trainingtag;
		this.exerciseList = exerciseList;
	}

	public String getTrainingtag() {
		return trainingtag;
	}

	public ArrayList<Properties> getExerciseList() {
		return exerciseList;
	}

}
