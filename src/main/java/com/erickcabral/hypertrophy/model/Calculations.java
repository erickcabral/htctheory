/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.model;

import java.text.DecimalFormat;
import java.util.ArrayList;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author sexta
 */
public class Calculations {

     String SET_HUNDRED = "100";
     String SET_ZERO = "";

     //////////////////////////// CALCULATIONS /////////////////////////////////////////////////////
     DecimalFormat decimalFormat = new DecimalFormat("00.#");

     public void inputFloatFilter(boolean isGroup, KeyEvent event) {
          // DecimalFormat decimalFormat = new DecimalFormat();
          // decimalFormat.applyPattern("00.#");
          TextField tf = (TextField) event.getSource();
          //String titlePaneGroup = tf.getParent().getParent().getParent().getParent().getId();
          int textFieldLenght = tf.getText().length();
          char valueInput = event.getCharacter().charAt(0);
          int decimal = 0;

          int maxValue;
          if (isGroup) {
               maxValue = 40; //Valor max p/ grupo
          }
          else {
               maxValue = 50;  //Valor max p/ grupo
          }

          if (valueInput == ',') {
               valueInput = '.';
               tf.deletePreviousChar();
               tf.appendText(String.valueOf(valueInput));
          }
          for (char decimalFinder : tf.getText().toCharArray()) {
               if (decimalFinder == '.' && valueInput != '\b') {
                    if (textFieldLenght == 1) {
                         tf.clear();
                         tf.appendText("00.");
                    }
                    else if (textFieldLenght == 2) {
                         if (Character.isDigit(valueInput)) {
                              tf.clear();
                              tf.appendText(String.format("0%c.", valueInput));
                         }
                         else if (valueInput == '.') {
                              tf.clear();
                              tf.appendText("00.");
                         }
                    }
                    decimal++;
               }
          }

          if (decimal > 1 && valueInput == '.') {
               tf.deletePreviousChar();
          }
          else if (!Character.isDigit(valueInput) && valueInput != '\r') {
               if (valueInput != '\b' && valueInput != '.') {
                    tf.deletePreviousChar();
               }
          }
          else {
               if (textFieldLenght > 4) {
                    tf.deletePreviousChar();
               }
               else if (textFieldLenght == 2) {
                    tf.appendText(".");
               }
               else if (textFieldLenght == 4) {
                    if (Float.valueOf(tf.getText()) > maxValue) {
                         tf.clear();
                         tf.appendText(String.valueOf(maxValue));
                    }
               }
          }
     }

     public boolean isIntFilterValid(boolean isGroup, KeyEvent event) {
          TextField tf = (TextField) event.getSource();
          String inputString = tf.getText();
          int textFieldLenght = inputString.length();
          char valueInput = event.getCharacter().charAt(0);

          if (valueInput == '\b' || valueInput == '\r') {
               return true;
          }
          if (!Character.isDigit(valueInput)) {
               tf.deletePreviousChar();
          }
          else {
               if (textFieldLenght > 2) {
                    if (!inputString.startsWith("100")) {
                         tf.deletePreviousChar();
                    }
               }
               return true;
          }
          return false;
     }

     public ValidUpdateReturn inputValueUpdate(boolean isGroup, boolean isSelectedGroup, TextField mainMuscTextField, TextField eventTextField, ArrayList<TextField> groupTextList) {
          String inputValueString = eventTextField.getText();
          int inputValueInt;
          int maxInput;
          if (isGroup) {
               maxInput = 40;
          }
          else {
               if (isSelectedGroup) {
                    maxInput = 50;
               }
               else {
                    maxInput = 100;
               }
          }

          if (inputValueString.isEmpty()) {
               inputValueInt = 0;
          }
          else {
               inputValueInt = Integer.valueOf(inputValueString);
          }

          int outputInt = inputValueInt;
          int total = 0;
          int minMainMuscleValue = 100 / groupTextList.size();
          System.out.println("MINIMO DO MAINMUSCLE : " + minMainMuscleValue);

          System.out.println("INPUT: " + inputValueString);
          for (TextField tfFinderField : groupTextList) {
               if (!tfFinderField.getId().equals(mainMuscTextField.getId())) {
                    if (!tfFinderField.getId().equals(eventTextField.getId())) {
                         int valueInt;
                         if (tfFinderField.getText().isEmpty()) {
                              valueInt = 0;
                         }
                         else {
                              valueInt = Integer.valueOf(tfFinderField.getText());
                         }
                         total += valueInt;
                         System.out.println(String.format("pesquisando: %s <%d> // total: %d", tfFinderField.getId(), valueInt, total));
                    }
               }
          }
          // Mx = [MaxInput -> 50 p/Musculos 40 p/Grupos] - Somatorio(Ma + Mb + Mc ... Mz) -> Valor maximo do input
          int inputUpdate = maxInput - total; // -> Checa Input
          if (inputValueInt > inputUpdate) {
               outputInt = inputUpdate;
          }
          int primaryValue = 100 - (total + outputInt); // -> Update do valor Principal
          int updateTotal = total + outputInt; // -> Update do Valor total Distribuido
          System.out.println("TOTAL :: " + total);
          System.out.println("UPDATE :: " + outputInt);
          System.out.println("UPDATE + TOTAL :: " + (total + outputInt));
          System.out.println("Primary >> " + primaryValue);

          return new ValidUpdateReturn(primaryValue, outputInt, updateTotal);
     }

     public float getMuscleRealPercentage(int groupValue, int muscleValue) {
          DecimalFormat df = new DecimalFormat("0.#");
          float realPercentage = Float.valueOf(muscleValue * groupValue) / 100;
          realPercentage = Float.valueOf(df.format(realPercentage).replace(',', '.'));
          //System.out.println("Group Value: " + groupValue + " Muscle Value: " + muscleValue + "Real: " + realPercentage);
          return realPercentage;
     }

     public class ValidUpdateReturn {

          private final int primaryUpdate;
          private final int validInput;
          private final int totalValue;

          public ValidUpdateReturn(int primaryUpdate, int validInput, int totalValue) {
               this.primaryUpdate = primaryUpdate;
               this.validInput = validInput;
               this.totalValue = totalValue;
          }

          public int getPrimaryUpdate() {
               return primaryUpdate;
          }

          public int getValidInput() {
               return validInput;
          }

          public int getTotalValue() {
               return totalValue;
          }

     }

}
