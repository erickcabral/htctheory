/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.model;

import java.util.ArrayList;
import static com.erickcabral.hypertrophy.model.Muscles.PECTORALS;

/**
 *
 * @author sexta
 */
public class Muscles {

	public Muscles() {
	//	this.setMuscleProperties();
	}

	public final String[] BODY_PART_STRING_LIST = new String[]{TORAX, ARMS, LEGS};

	/////Muscles Variables//////////////
	public final static String TORAX = "Torax";
	public final static String ARMS = "Arms";
	public final static String ARMS_LEFT = "LeftArm";
	public final static String ARMS_RIGHT = "RightArm";

	public final static String WAIST = "Waist";

	public final static String LEGS = "Legs";
	public final static String THIGH_LEFT = "LeftThigh";
	public final static String THIGH_RIGHT = "RightThigh";
	public final static String CALF_LEFT = "LeftCalf";
	public final static String CALF_RIGHT = "RightCalf";

	public final static String SHOULDERS = "Shoulders";
	public final static String SH_DELTOID_FRONT = "Deltoid Front";
	public final static String SH_DELTOID_MED = "Deltoid Med";
	public final static String SH_DELTOID_BACK = "Deltoid Back";
	public final static String SH_TRAPEZIUS = "Trapezius";
	public final static String SH_INFRASPINATUS = "Infraspinatus";
	public final static String SH_TERES = "Teres Set";

	private final static ArrayList<String> SHOULDER_MAIN_MUSCLES = new ArrayList<>() {
		{
			add(SH_DELTOID_FRONT);
			add(SH_DELTOID_MED);
			add(SH_DELTOID_BACK);
			add(SH_TRAPEZIUS);
			add(SH_INFRASPINATUS);
			add(SH_TERES);
		}
	};

	public final static String PECTORALS = "Pectorals";
	public final static String PEC_UPPER = "Pectoral Upper";
	public final static String PEC_MAJOR = "Pectoral Major";
	public final static String PEC_LOWER = "Pectoral Lower";
	public final static String PEC_INNER = "Pectoral Center";

	private final static ArrayList<String> PECTORALS_MAIN_MUSCLES = new ArrayList<>() {
		{
			add(PEC_UPPER);
			add(PEC_MAJOR);
			add(PEC_LOWER);
			add(PEC_INNER);
		}
	};

	public final static String BACK = "Back";
	public final static String BAK_DORSI = "Traps"; // "Latissimus Dorsi";
	public final static String BAK_ERECTOR = "Erector"; //Erector Spinae";

	private final static ArrayList<String> BACK_MAIN_MUSCLES = new ArrayList<>() {
		{
			add(BAK_DORSI);
			add(BAK_ERECTOR);
		}
	};

	public final static String ABS = "Abs";
	public final static String ABS_UPPER = "Abs Upper";
	public final static String ABS_LOWER = "Abs Lower";
	public final static String ABS_OBLIQUE = "Abs Oblique";
	public final static String ABS_RECTUS = "Abs Rectus";

	private final static ArrayList<String> ABS_MAIN_MUSCLES = new ArrayList<>() {
		{
			add(ABS_UPPER);
			add(ABS_RECTUS);
			add(ABS_LOWER);
			add(ABS_OBLIQUE);
		}
	};

	public final static String BICEPS = "Biceps";
	public final static String BIC_LONG_HEAD = "Biceps Long"; //Long Head";
	public final static String BIC_SHORT_HEAD = "Biceps Short"; //Short Head";

	private final static ArrayList<String> BICEPS_MAIN_MUSCLES = new ArrayList<>() {
		{
			add(BIC_LONG_HEAD);
			add(BIC_SHORT_HEAD);
		}
	};

	public final static String TRICEPS = "Triceps";
	public final static String TRI_INNER_HEAD = "Triceps Inner"; //"Inter Head";
	public final static String TRI_LATERAL_HEAD = "Triceps Lateral"; //"Lateral Head";
	public final static String TRI_MEDIAL_HEAD = "Triceps Medial"; //"Medial Head";

	private final static ArrayList<String> TRICEPS_MAIN_MUSCLES = new ArrayList<>() {
		{
			add(TRI_INNER_HEAD);
			add(TRI_LATERAL_HEAD);
			add(TRI_MEDIAL_HEAD);
		}
	};

	public final static String GLUTES = "Glutes";
	public final static String GL_MAXIMUS = "Gluteus Maximus";
	public final static String GL_MEDIUS = "Gluteus Medius";

	private final static ArrayList<String> GLUTES_MAIN_MUSCLES = new ArrayList<>() {
		{
			add(GL_MAXIMUS);
			add(GL_MEDIUS);
		}
	};

	public final static String THIGHS = "Thighs";

	public final static String THIGHS_FRONT = "Front Thighs";
	public final static String THG_RETUS_FEMUROUS = "Retus Fem."; //"Retus Femorous";
	public final static String THG_VASTUS_LATERALIS = "Vastus Lat."; //"Vastus Lataralis";
	public final static String THG_VASTUS_MEDIALIS = "Vastus Med."; //"Vastus Medialus";

	public final String[] frontThighMuscles = new String[]{THG_RETUS_FEMUROUS, THG_VASTUS_LATERALIS, THG_VASTUS_MEDIALIS};

	private final static ArrayList<String> THIGHS_FRONT_MAIN_MUSCLES = new ArrayList<>() {
		{
			add(THG_RETUS_FEMUROUS);
			add(THG_VASTUS_LATERALIS);
			add(THG_VASTUS_MEDIALIS);
		}
	};

	public final static String THIGHS_BACK = "Back Thighs";
	public final static String THG_BICEPS_FEMORIS = "Biceps Fem."; //"Biceps Femoris";
	private final static String THIGHS_BACK_MAIN_MUSCLES = THG_BICEPS_FEMORIS;

	public final static String THIGHS_INNER = "Inner Thighs";
	public final static String THG_ADDUCTOR = "Adductor";

	private final static String THIGHS_INNER_MAIN_MUSCLES = THG_ADDUCTOR;

	public final static String THIGHS_OUTTER = "Outter Thighs";
	public final static String THG_ABDUCTOR = "Abductor";

	private final static String THIGHS_OUTTER_MAIN_MUSCLES = THG_ABDUCTOR;

	private final static ArrayList<String> THIGHS_MAIN_MUSCLES = new ArrayList<>() {
		{
			add(THIGHS_BACK_MAIN_MUSCLES);
			addAll((THIGHS_FRONT_MAIN_MUSCLES));
			add((THIGHS_INNER_MAIN_MUSCLES));
			add(THIGHS_OUTTER_MAIN_MUSCLES);
		}
	};

	public final static String CALVES = "Calves";
	public final static String CAL_GAST_LAT_HEAD = "Gast. Lat."; // "Gast.Lateralis";
	public final static String CAL_GAST_INNER_HEAD = "Gast. Med."; //"Gast.Medialis";
	private final static ArrayList<String> CALVES_MAIN_MUSCLES = new ArrayList<>() {
		{
			add(CAL_GAST_LAT_HEAD);
			add(CAL_GAST_INNER_HEAD);
		}
	};

	public static final String[] GROUP_STRING_LIST = {SHOULDERS,
		PECTORALS, BACK,
		ABS, BICEPS,
		TRICEPS, GLUTES,
		THIGHS, CALVES
	};

	
	
/*	
	///------------- NOVO BUSCADOR DE GRUPOS --------------------//
	public final static Properties MUSCLE_PROPERTIES = new Properties();

	public Properties getMusclesProperties() {
		if (MUSCLE_PROPERTIES.stringPropertyNames().isEmpty()) {
			setMuscleProperties();
		}
		return MUSCLE_PROPERTIES;
	}

	private void setMuscleProperties() {
		for (String getter : SHOULDER_MAIN_MUSCLES) {
			MUSCLE_PROPERTIES.setProperty(getter, SHOULDERS);
			System.out.println("Muscle Prop Add: " + SHOULDERS + "-> " + getter);
		}
		for (String getter : PECTORALS_MAIN_MUSCLES) {
			MUSCLE_PROPERTIES.setProperty(getter, PECTORALS);
			System.out.println("Muscle Prop Add: " + PECTORALS + "-> " + getter);
		}
		for (String getter : BACK_MAIN_MUSCLES) {
			MUSCLE_PROPERTIES.setProperty(getter, BACK);
			System.out.println("Muscle Prop Add: " + BACK + "-> " + getter);

		}
		for (String getter : BICEPS_MAIN_MUSCLES) {
			MUSCLE_PROPERTIES.setProperty(getter, BICEPS);
			System.out.println("Muscle Prop Add: " + BICEPS + "-> " + getter);

		}
		for (String getter : TRICEPS_MAIN_MUSCLES) {
			MUSCLE_PROPERTIES.setProperty(getter, TRICEPS);
			System.out.println("Muscle Prop Add: " + TRICEPS + "-> " + getter);

		}
		for (String getter : ABS_MAIN_MUSCLES) {
			MUSCLE_PROPERTIES.setProperty(getter, ABS);
			System.out.println("Muscle Prop Add: " + ABS + "-> " + getter);

		}
		for (String getter : GLUTES_MAIN_MUSCLES) {
			MUSCLE_PROPERTIES.setProperty(getter, GLUTES);
			System.out.println("Muscle Prop Add: " + GLUTES + "-> " + getter);

		}
		for (String getter : THIGHS_MAIN_MUSCLES) {
			MUSCLE_PROPERTIES.setProperty(getter, THIGHS);
			System.out.println("Muscle Prop Add: " + THIGHS + "-> " + getter);

		}
		for (String getter : CALVES_MAIN_MUSCLES) {
			MUSCLE_PROPERTIES.setProperty(getter, CALVES);
			System.out.println("Muscle Prop Add: " + CALVES + "-> " + getter);
		}
	}

	public String findGroupByMuscle(String muscle) {
		if (this.MUSCLE_PROPERTIES.containsKey(muscle)) {
			return this.MUSCLE_PROPERTIES.getProperty(muscle);
		}
		return null;
	}
*/
	
	
	///MuscleGroup Itens///
	public ArrayList<String> getMuscleGroupStringList(String bodyPart) {
		switch (bodyPart) {
		case TORAX:
			return STRING_LIST_TORAX;
		case ARMS:
			return STRING_LIST_ARMS;
		case LEGS:
			return STRING_LIST_LEGS;
		default:
			return null;
		}
	}
	
	///MainMuscles Strings Items///
	public ArrayList<String> getMainMusclesStringList(String muscleGroup) {
		switch (muscleGroup) {
		case SHOULDERS:
			return SHOULDER_MAIN_MUSCLES;
		case PECTORALS:
			return PECTORALS_MAIN_MUSCLES;
		case BACK:
			return BACK_MAIN_MUSCLES;
		case ABS:
			return ABS_MAIN_MUSCLES;
		case BICEPS:
			return BICEPS_MAIN_MUSCLES;
		case TRICEPS:
			return TRICEPS_MAIN_MUSCLES;
		case GLUTES:
			return GLUTES_MAIN_MUSCLES;
		case THIGHS:
			return THIGHS_MAIN_MUSCLES;
		case CALVES:
			return CALVES_MAIN_MUSCLES;
		default:
			return null;
		}
	}
	
	
	public String findMuscleGroup (String mainMuscle){
		if(SHOULDER_MAIN_MUSCLES.contains(mainMuscle)){
			return SHOULDERS;
		}else if(PECTORALS_MAIN_MUSCLES.contains(mainMuscle)){
			return PECTORALS;
		}else if(BACK_MAIN_MUSCLES.contains(mainMuscle)){
			return BACK;
		}else if(ABS_MAIN_MUSCLES.contains(mainMuscle)){
			return ABS;
		}else if(BICEPS_MAIN_MUSCLES.contains(mainMuscle)){
			return BICEPS;
		}else if(TRICEPS_MAIN_MUSCLES.contains(mainMuscle)){
			return TRICEPS;
		}else if(GLUTES_MAIN_MUSCLES.contains(mainMuscle)){
			return GLUTES;
		}else if(THIGHS_MAIN_MUSCLES.contains(mainMuscle)){
			return THIGHS;
		}else if(CALVES_MAIN_MUSCLES.contains(mainMuscle)){
			return CALVES;
		}else{
			throw new NullPointerException("GROUP NOT FOUND FOR MAIN MUSCLE: " + mainMuscle);
		}
	}

	/*
	private ArrayList<MusclesData> shoulderSet = new ArrayList() {
		{
			add(new MusclesData(SH_DELTOID_FRONT, 0));
			add(new MusclesData(SH_DELTOID_MED, 0));
			add(new MusclesData(SH_DELTOID_BACK, 0));
			add(new MusclesData(SH_TRAPEZIUS, 0));
			add(new MusclesData(SH_INFRASPINATUS, 0));
			add(new MusclesData(SH_TERES, 0));

		}
	};

	private ArrayList<MusclesData> pectoralSet = new ArrayList() {
		{
			add(new MusclesData(PEC_UPPER, 0));
			add(new MusclesData(PEC_MAJOR, 0));
			add(new MusclesData(PEC_LOWER, 0));
			add(new MusclesData(PEC_INNER, 0));
		}
	};

	private ArrayList<MusclesData> backSet = new ArrayList() {
		{
			add(new MusclesData(BAK_DORSI, 0));
			add(new MusclesData(BAK_ERECTOR, 0));
		}
	};

	private ArrayList<MusclesData> abdominalSet = new ArrayList() {
		{
			add(new MusclesData(ABS_UPPER, 0));
			add(new MusclesData(ABS_LOWER, 0));
			add(new MusclesData(ABS_OBLIQUE, 0));
			add(new MusclesData(ABS_RECTUS, 0));
		}
	};

	private ArrayList<MusclesData> bicepsSet = new ArrayList() {
		{
			add(new MusclesData(BIC_LONG_HEAD, 0));
			add(new MusclesData(BIC_SHORT_HEAD, 0));
		}
	};

	private ArrayList<MusclesData> tricepsSet = new ArrayList() {
		{
			add(new MusclesData(TRI_INNER_HEAD, 0));
			add(new MusclesData(TRI_LATERAL_HEAD, 0));
			add(new MusclesData(TRI_MEDIAL_HEAD, 0));
		}
	};

	private ArrayList<MusclesData> glutesSet = new ArrayList() {
		{
			add(new MusclesData(GL_MAXIMUS, 0));
			add(new MusclesData(GL_MEDIUS, 0));
		}
	};

	private ArrayList<MusclesData> thighsSet = new ArrayList() {
		{
			add(new MusclesData(THG_RETUS_FEMUROUS, 0));
			add(new MusclesData(THG_VASTUS_LATERALIS, 0));
			add(new MusclesData(THG_VASTUS_MEDIALIS, 0));
			add(new MusclesData(THG_BICEPS_FEMORIS, 0));
			add(new MusclesData(THG_ADDUCTOR, 0));
			add(new MusclesData(THG_ABDUCTOR, 0));
		}
	};

	private ArrayList<MusclesData> calvesSet = new ArrayList() {
		{
			add(new MusclesData(CAL_GAST_LAT_HEAD, 0));
			add(new MusclesData(CAL_GAST_INNER_HEAD, 0));
		}
	};
*/
	
	
	
	/////////////////// Muscle Groups List ////////////////////////////// 
	public static final ArrayList<String> STRING_LIST_TORAX = new ArrayList() {
		{
			add(SHOULDERS);
			add(PECTORALS);
			add(BACK);
			add(ABS);
		}
	};
	public static final ArrayList<String> STRING_LIST_ARMS = new ArrayList() {
		{
			add(BICEPS);
			add(TRICEPS);
		}
	};
	public static final ArrayList<String> STRING_LIST_LEGS = new ArrayList() {
		{
			add(GLUTES);
			add(THIGHS);
			add(CALVES);
		}
	};

	public static final ArrayList<String> STRING_LIST_GROUPS = new ArrayList() {
		{
			addAll(STRING_LIST_TORAX);
			addAll(STRING_LIST_ARMS);
			addAll(STRING_LIST_LEGS);
		}
	};

	public static final ArrayList<String> STRING_LIST_MAIN_MUSCLES = new ArrayList() {
		{
			addAll(SHOULDER_MAIN_MUSCLES);
			addAll(PECTORALS_MAIN_MUSCLES);
			addAll(BACK_MAIN_MUSCLES);
			addAll(ABS_MAIN_MUSCLES);
			addAll(BICEPS_MAIN_MUSCLES);
			addAll(TRICEPS_MAIN_MUSCLES);
			addAll(GLUTES_MAIN_MUSCLES);
			addAll(THIGHS_MAIN_MUSCLES);
			addAll(CALVES_MAIN_MUSCLES);
		}
	};

/*	
	public class MusclesData {

		String muscleName;
		Float valueFloat = 0f;

		public MusclesData(String muscleName, float muscleValue) {
			this.muscleName = muscleName;
			this.valueFloat = muscleValue;
		}

		public String getMuscleName() {
			return muscleName;
		}

		public void setMuscleName(String muscleName) {
			this.muscleName = muscleName;
		}

		public float getValueFloat() {
			return valueFloat;
		}

		public void setValueFloat(float valueInt) {
			this.valueFloat = valueInt;
		}
	}

*/
}
