/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.model;

import com.erickcabral.hypertrophy.application.Hypertrophy;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Properties;
import javafx.stage.Stage;
import com.erickcabral.hypertrophy.supportClasses.FilesHandlers;

/**
 *
 * @author Erick Cabral
 */
public class WeekTrainingSetBuilder extends FilesHandlers implements Serializable {

	private final String WEEKSET_DEFAULT_FOLDER = "F:\\Cursos\\JAVA SE\\Projetos\\Hypertrophy\\FILES\\WEEK_SETS"; // Remover Após Criar Prop de Config do Sotware
	
	public static final String FILE_TYPE = "Week Training Set";
	
	public static String KEY_DAY = "Week Day";
	private String weekTrainingTag;

	public String getWeekTrainingTag() {
		return weekTrainingTag;
	}

	public void setWeekTrainingTag(String weekTrainingTag) {
		this.weekTrainingTag = weekTrainingTag;
	}
	private String weekDay;
	private ArrayList<Properties> weekTrainingList;

	public ArrayList<Properties> getWeekTrainingList() {
		return weekTrainingList;
	}

	public void setWeekTrainingList(ArrayList<Properties> weekTrainingList) {
		this.weekTrainingList = weekTrainingList;
	}

	public String getWeekDay() {
		return weekDay;
	}

	public void setWeekDay(String weekDay) {		
		this.weekDay = weekDay;
	}
	
	public WeekTrainingSetBuilder(Stage mainStage) {
		super(mainStage);
	}
	
	public boolean save(String weekTrainingSetTag) {
		File fileInfo = createFileToSave(weekTrainingSetTag, this.FILE_TYPE, this.WEEKSET_DEFAULT_FOLDER); //fileType = Week Training List
		if (fileInfo != null) {
			System.out.println("Training Exercise List Saved  -> " + this.weekTrainingList);
			return Hypertrophy.getInstance().getSerialization().saveWeekSetList(this, fileInfo);
		} else {
			System.out.println(" File Info -> null");
		}
		return false;
	}
}
