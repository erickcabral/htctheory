/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

/**
 *
 * @author sexta
 */
public class AthleteManagement {

	private final String WARNING_DATE = "Inicial Date";
	private final String WARNING_NAME = "Must be more then 3 characteres";
	private final String SET_ZERO = "0.0";

	public static final String DATE_INPUT = "DATE";
	// public static final String DATE_UPDATE = "LAST_UPDATE";
	public static final String DATE_FORMAT = "dd/MM/YYYY";
	private String DATE_FORMATTED;
	private DatePicker datePicker;

	private Properties athleteProperties;

	private ObservableList<TextField> tfList = FXCollections.observableArrayList();

	public void setTfList(ObservableList<TextField> tfList) {
		this.tfList = tfList;
	}

	private List<Properties> athleteDataList = new ArrayList<>();

	public List<Properties> getAthleteDataList() {
		return athleteDataList;
	}

	public Properties getAthleteProperties() {
		return athleteProperties;
	}

	public void addFieldToTfList(TextField textField, String tfID) {
		textField.setId(tfID);
		if (!tfList.contains(textField)) {
			tfList.add(textField);
		} else {
			System.out.println("TextField Already in List");
		}
	}
/*

	public boolean inputDatePicker(DatePicker inputDatePicker) {
		datePicker = inputDatePicker;
		if (datePicker.getValue() == null || datePicker.getValue().toString().equals(WARNING_DATE)) {
			datePicker.setPromptText("Set Inicial Date");
			return false;
		} else {
			DATE_FORMATTED = datePicker.getValue().format(DateTimeFormatter.ISO_DATE);
		}
		return true;
	}

	public void inputNameFilter(TextField inputTextField) {
		int inputSize = inputTextField.getText().length();
		if (inputSize < 3 || inputTextField.getText().equals(this.WARNING_NAME) || inputTextField.getText().equals("")) {
			inputTextField.setText(WARNING_NAME);
		}
	}

	public void checkInputs(TextField textField) {
		for (TextField finder : this.tfList) {
			if (!finder.getId().equals(textField.getId()) && !finder.getText().isEmpty()) {
				if (finder.getId().equals(AthleteProfile.ATHLETE_NAME) || finder.getId().equals(AthleteProfile.ATHLETE_SURNAME)) {
					inputNameFilter(finder);
				} else {
					inputFilter(finder);
				}
			}
		}
	}
*/
	
/*	
	public boolean setNewAthleteProperties() {
		boolean blankField = false;
		boolean propsOK = false;

		if (inputDatePicker(datePicker)) {
			for (TextField finder : tfList) {
				checkInputs(finder);
				// System.out.println(String.format("Key: %s Value: %s", finder.getId(), finder.getText()));
				if (finder.getText().equals(SET_ZERO) || finder.getText().equals(WARNING_NAME) || finder.getText().isEmpty()) {
					finder.clear();
					blankField = true;
				} else {
					// finder.setDisable(true);
				}
			}
			if (!blankField) {
				AthleteProfile newAthleteProfile = new AthleteProfile();
				athleteProperties = new Properties();
				athleteProperties.setProperty(DATE_INPUT, DATE_FORMATTED);
				for (TextField setter : tfList) {
					athleteProperties.setProperty(setter.getId(), setter.getText());
				}
				newAthleteProfile.addAthlete(athleteProperties);
				propsOK = true;
			}
		} else {
			System.out.println("FILL UP THE BLANK FIELDS TO PROCEED");
		}
		return propsOK;
	}

	public boolean setUpdateAthleteProperties(DatePicker updatePicker) {
		boolean blankField = false;
		boolean propsOK = false;

		if (inputDatePicker(updatePicker)) {
			for (TextField finder : tfList) {
				checkInputs(finder);
				// System.out.println(String.format("Key: %s Value: %s", finder.getId(), finder.getText()));
				if (finder.getText().equals(SET_ZERO) || finder.getText().equals(WARNING_NAME) || finder.getText().isEmpty()) {
					finder.clear();
					blankField = true;
				} else {
					// finder.setDisable(true);
				}
			}
			if (!blankField) {
				athleteProperties = new Properties();
				athleteProperties.setProperty(DATE_INPUT, DATE_FORMATTED);
				for (TextField setter : tfList) {
					athleteProperties.setProperty(setter.getId(), setter.getText());
					System.out.println(String.format("KEY: %s VALUE: %s", setter.getId(), setter.getText()));
				}
				Hypertrophy.getInstance().getSelectedProfile().addPropertieToProfile(athleteProperties);
				Hypertrophy.getInstance().getWindowManager().getMainProgramController().loadProfile(Hypertrophy.getInstance().getSelectedProfile());
				propsOK = true;
			}
		} else {
			System.out.println("FILL UP THE BLANK FIELDS TO PROCEED");
		}
		return propsOK;
	}

	public boolean setEditedAthleteProperties(DatePicker datePicker, int position) {
		boolean blankField = false;
		boolean propsOK = false;

		if (inputDatePicker(datePicker)) {
			for (TextField finder : tfList) {
				checkInputs(finder);
				// System.out.println(String.format("Key: %s Value: %s", finder.getId(), finder.getText()));
				if (finder.getText().equals(SET_ZERO) || finder.getText().equals(WARNING_NAME) || finder.getText().isEmpty()) {
					finder.clear();
					blankField = true;
				} else {
					// finder.setDisable(true);
				}
			}
			if (!blankField) {
				athleteProperties = Hypertrophy.getInstance().getSelectedProfile().getPropertyList().get(position);
				athleteProperties.setProperty(DATE_INPUT, DATE_FORMATTED);
				for (TextField setter : tfList) {
					athleteProperties.setProperty(setter.getId(), setter.getText());
					System.out.println(String.format("POSIÇÃO %d \nKEY: %s VALUE: %s", position, setter.getId(), setter.getText()));
				}
				Hypertrophy.getInstance().getSelectedProfile().addEditedProperty(athleteProperties, position);
				Hypertrophy.getInstance().getWindowManager().getMainProgramController().loadProfile(Hypertrophy.getInstance().getSelectedProfile());
			}
		} else {
			System.out.println("FILL UP THE BLANK FIELDS TO PROCEED");
		}
		return propsOK;
	}
*/
}
