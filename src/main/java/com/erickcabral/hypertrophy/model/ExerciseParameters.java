/*
 * To change this license header); add(choose License Headers in Project Properties.
 * To change this template file); add(choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.model;

import com.erickcabral.hypertrophy.application.Hypertrophy;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Properties;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author sexta
 */
public class ExerciseParameters extends Muscles implements Serializable {

	public static final String EXERCISE_NAME = "Name";
	public static final String EXERCISE_BODYPART = "Body Part";
	public static final String EXERCISE_MUSCLE_GROUP = "Group";
	public static final String EXERCISE_MAIN_MUSCLE = "Main Muscle";

	private final Properties exerciseProperties = new Properties();

	private String exerciseName;
	private String exerciseBodyPart;
	private String exerciseMuscleGroup;
	private String exerciseMainMuscle;

	public static final String[] BASIC_INFO_STRING_LIST = {EXERCISE_NAME, EXERCISE_BODYPART, EXERCISE_MUSCLE_GROUP, EXERCISE_MAIN_MUSCLE};

	public ExerciseParameters() {
		for (String muscleGroupGetter : STRING_LIST_GROUPS) {
			this.exerciseProperties.setProperty(muscleGroupGetter, "0.0");
		}
		for (String mainMusclesGetter : STRING_LIST_MAIN_MUSCLES) {
			this.exerciseProperties.setProperty(mainMusclesGetter, "0.0");
		}
	}

	public void setExerciseName(String exerciseName) {
		this.exerciseName = exerciseName;
		this.exerciseProperties.put(EXERCISE_NAME, this.exerciseName);
	}

	public void setExerciseBodyPart(String exerciseBodyPart) {
		this.exerciseBodyPart = exerciseBodyPart;
		this.exerciseProperties.put(EXERCISE_BODYPART, this.exerciseBodyPart);
	}

	public void setExerciseMuscleGroup(String exerciseMuscleGroup) {
		this.exerciseMuscleGroup = exerciseMuscleGroup;
		this.exerciseProperties.put(EXERCISE_MUSCLE_GROUP, this.exerciseMuscleGroup);
	}

	public void setExerciseMainMuscle(String exerciseMainMuscle) {
		this.exerciseMainMuscle = exerciseMainMuscle;
		this.exerciseProperties.put(EXERCISE_MAIN_MUSCLE, this.exerciseMainMuscle);
	}

	public void setGroupPercentages(Properties groupValues) {
		for (String groupGetter : groupValues.stringPropertyNames()) {
			this.exerciseProperties.put(groupGetter, groupValues.getProperty(groupGetter));
		}
	}

	public void setMainMusclesPercentages(Properties mainMusclesValues) {
		for (String groupGetter : mainMusclesValues.stringPropertyNames()) {
			this.exerciseProperties.put(groupGetter, mainMusclesValues.getProperty(groupGetter));
		}
	}

	public boolean saveExerciseParameters() {
		return Hypertrophy.getInstance().getSerialization().saveExercise(this.exerciseProperties);
	}

	//Exercise Params Adapter para TABLEVIEWS
	public ArrayList<SimpleStringProperty> importPropertiesFile(Properties properties) {
		ArrayList<SimpleStringProperty> simpleStringKeysList = new ArrayList<>();
		for (String getter : properties.stringPropertyNames()) {
			SimpleStringProperty keySimpleString = new SimpleStringProperty();
			keySimpleString.set(getter);
			keySimpleString.setValue(properties.getProperty(getter));
			simpleStringKeysList.add(keySimpleString);
			System.out.println("Imported > " + keySimpleString.getName() + "<" + keySimpleString.getValue() + ">");
		}
		return simpleStringKeysList;
	}

	public void getRealMainMusclePercentages() {
		Properties realPercentages = new Properties();
		for(String mainMuscleGetter : STRING_LIST_MAIN_MUSCLES){
			float mainMuscleValue = Float.valueOf(this.exerciseProperties.getProperty(mainMuscleGetter));
			if(mainMuscleValue != 0f){
				System.out.println(String.format("Muscle: %s Value: %f", mainMuscleGetter, mainMuscleValue));
				String muscleGroup = this.findMuscleGroup(mainMuscleGetter);
				String muscleGroupValue = this.exerciseProperties.getProperty(muscleGroup);
				float groupPercentage = Float.valueOf(muscleGroupValue);
				float muscleRealPercentage = (mainMuscleValue * groupPercentage)/100;
				System.out.println(String.format("Muscle: %s Real Value: %f", mainMuscleGetter, muscleRealPercentage));
				realPercentages.put(mainMuscleGetter, String.valueOf(muscleRealPercentage));
			}
		}
		System.out.println("REAL PERCENT PROPS -> " + realPercentages);
	}

}
