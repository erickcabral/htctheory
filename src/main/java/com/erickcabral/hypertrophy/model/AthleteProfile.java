/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.model;

import com.erickcabral.hypertrophy.application.Hypertrophy;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Properties;

/**
 *
 * @author sexta
 */
public class AthleteProfile implements Serializable {

	private String athleteName;
	private String athleteSurname;
	private int athleteID = 0;

	public static final String ATHLETE_NAME = "NAME";
	public static final String ATHLETE_SURNAME = "SURNAME";
	public static final String ATHLETE_ID = "ID";
	public static final String ATHLETE_WEIGHT = "WEIGHT";
	public static final String ATHLETE_HEIGHT = "HEIGHT";
	public static final String ATHLETE_FAT = "FAT%";

	public static final String DATE_INPUT = "DATE";

	private ArrayList<Properties> measuresList = new ArrayList<>();

	public AthleteProfile() {
	}

	public int getAthleteID() {
		//return Integer.valueOf(this.measuresList.get(0).getProperty(ATHLETE_ID));
		return this.athleteID;
	}

	public void setAthleteID(int athleteID) {
		this.athleteID = athleteID;
	}

	public String getAthleteName() {
		return athleteName;
	}

	public String getAthleteSurname() {
		return athleteSurname;
	}

	public void saveAthlete(Properties properties) { // REVISAR ESSE METODO	
		this.athleteName = properties.getProperty(ATHLETE_NAME);
		this.athleteSurname = properties.getProperty(ATHLETE_SURNAME);
		this.getInicialMeasures(properties);
		Hypertrophy.getInstance().getSerialization().saveAthlete(this);
	}

	public void getInicialMeasures(Properties property) {// Funando Ok
		this.measuresList.add(property);
		Collections.sort(this.measuresList, new Comparator<Properties>() {
			@Override
			public int compare(Properties o1, Properties o2) {
				LocalDate date1 = LocalDate.parse(o1.getProperty(DATE_INPUT));
				LocalDate date2 = LocalDate.parse(o2.getProperty(DATE_INPUT));
				return date1.compareTo(date2);
			}
		});
	}

	public int getMeasurePosition(String valueLocator) {
		int position = -1;
		for (Properties finder : this.measuresList) {
			position++;
			if (finder.contains(valueLocator)) {
				break;
			}
		}
		return position;
	}

	public void saveMeasureEdition(Properties editedProperties, int position) { //TODO: Revisar isso -> Fazer um sort no Array
		System.out.println("MEASURE POSITION -> " + position);
		System.out.println("PROS BEFORE -> " + this.measuresList.get(position));
		this.measuresList.get(position).clear();
		this.measuresList.get(position).putAll(editedProperties);
		Collections.sort(this.measuresList, new Comparator<Properties>() {
			@Override
			public int compare(Properties o1, Properties o2) {
				LocalDate date1 = LocalDate.parse(o1.getProperty(DATE_INPUT));
				LocalDate date2 = LocalDate.parse(o2.getProperty(DATE_INPUT));
				return date1.compareTo(date2);
			}
		});
		System.out.println("PROS AFTER -> " + this.measuresList.get(position));
		if (position == 0) {
			this.athleteName = editedProperties.getProperty(ATHLETE_NAME);
			this.athleteSurname = editedProperties.getProperty(ATHLETE_SURNAME);
		}
		Hypertrophy.getInstance().getSerialization().saveAthlete(this);
	}

	public ArrayList<Properties> getMeasuresList() {
		return this.measuresList;
	}

	public void updateAthlete(Properties profile) { //Atualiza a lista de informações do atleta
		System.out.println("Atualizar o profile deste Atleta > finder.addPropertieToProfile(profile)");
	}
}
