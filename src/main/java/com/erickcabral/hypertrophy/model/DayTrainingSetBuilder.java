/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.model;

import com.erickcabral.hypertrophy.application.Hypertrophy;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Properties;
import javafx.stage.Stage;
import com.erickcabral.hypertrophy.supportClasses.FilesHandlers;

/**
 *
 * @author Erick Cabral
 */
public class DayTrainingSetBuilder implements Serializable {

	public static final String DAYSET_DEFAULT_FOLDER = "F:\\Cursos\\JAVA SE\\Projetos\\Hypertrophy\\FILES\\DAY_SETS"; // Remover Após Criar Prop de Config do Sotware
	public static final String WEEK_DAY = "WEEK_DAY";
	public static String TRAINING_TAG = "TRAINING_TAG";

	private String trainingTagName;
	private Properties dayTrainingSetProperties = new Properties();
	private ArrayList<Properties> exerciseList = new ArrayList<>();

	public Properties getDayTrainingSetProperties() {
		return dayTrainingSetProperties;
	}
	
	public void setWeekDay(String day){
		this.dayTrainingSetProperties.setProperty(WEEK_DAY, day);
	}

	public void setDayTrainingSetProperties(Properties dayTrainingSetProperties) {
		this.dayTrainingSetProperties = dayTrainingSetProperties;
	}

	public String getTrainingTagName() {
		return trainingTagName;
	}

	public void setTrainingTagName(String trainingTagName) {
		this.trainingTagName = trainingTagName;
	}

	public ArrayList<Properties> getExerciseList() {
		return exerciseList;
	}

	public void setExerciseList(ArrayList<Properties> exerciseList) {
		this.exerciseList = exerciseList;
	}
	
	public void clearTrainingSetList(){
		this.exerciseList.clear();
	}

	public void saveTraining(String trainingTagName, Properties dayTrainingProperties) { //Futuramente Salvar Listas Semanais Separadas, em caso de mudança de treino mensal
		this.trainingTagName = trainingTagName;
		this.dayTrainingSetProperties = dayTrainingProperties;
		this.dayTrainingSetProperties.setProperty(DayTrainingSetBuilder.TRAINING_TAG, this.trainingTagName);
		System.out.println("TRAING TAG -> " + trainingTagName);
		//// New Saving///
		Stage mainStage = Hypertrophy.getInstance().getWindowManager().getMainStage();
		FilesHandlers filesHandlers = new FilesHandlers(mainStage);
		System.out.println("TRAING PROPS -> " + this.dayTrainingSetProperties);
		File fileInfo = filesHandlers.createFileToSave(this.trainingTagName, TrainingSetList.FILE_TYPE, this.DAYSET_DEFAULT_FOLDER); //fileType = Training List

		if (fileInfo != null) {
			Hypertrophy.getInstance().getSerialization().saveDaySetList(this, fileInfo);
		} else {
			System.out.println(" File Info -> null");
		}

	}
}
