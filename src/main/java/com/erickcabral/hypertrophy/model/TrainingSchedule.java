/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.hypertrophy.model;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Erick Cabral
 */
public class TrainingSchedule {

	DayTraining dayTraining;
	String trainingTag = "trainging Tag";

	ObservableList<DayTraining> weekSchedule = FXCollections.observableArrayList();

	public void addDayTraining(int weekDay, String traingingTag, List<ExerciseParameters> dayList) {
		dayTraining = new DayTraining(weekDay, trainingTag, dayList);
		weekSchedule.add(weekDay, dayTraining);
	}

	public void listWeekExercises(int weekDay) {

		System.out.println("SALVO -> " + dayTraining.getTraining_Tag());
		System.out.println("SALVO -> " + dayTraining.getDayExerciseList().get(weekDay).toString());
		System.out.println("SALVO -> " + dayTraining.getDayExerciseList().toString());
	}

	private class DayTraining {

		private String training_Tag = "default";
		private int trainging_day = 0;
		private List<ExerciseParameters> dayExerciseList = new ArrayList<>();

		public DayTraining(int weekDay, String tag, List<ExerciseParameters> exerciseList) {
			this.trainging_day = weekDay;
			this.training_Tag = tag;
			this.dayExerciseList = exerciseList;
		}

		public String getTraining_Tag() {
			return training_Tag;
		}

		public void setTraining_Tag(String training_Tag) {
			this.training_Tag = training_Tag;
		}

		public int getTrainging_day() {
			return trainging_day;
		}

		public void setTrainging_day(int trainging_day) {
			this.trainging_day = trainging_day;
		}

		public List<ExerciseParameters> getDayExerciseList() {
			return dayExerciseList;
		}

		public void setDayExerciseList(List<ExerciseParameters> dayExerciseList) {
			this.dayExerciseList = dayExerciseList;
		}
	}

}
